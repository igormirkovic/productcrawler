const TinexCrawler = require('./crawler/Tinex/tinex');
const PaketCrawler = require('./crawler/Paket/paketi');
const ReptilCrawler = require('./crawler/Reptil/reptil');
const express = require('express');
const queryString = require('query-string');
var SourceDBHelper = require('./SourceDBHelper');
var CrawlerHelper = require('./CrawlerHelper')
var CrawlerObj = new CrawlerHelper()
var dbHelper = new SourceDBHelper();
var dbHelperCrawl = new SourceDBHelper(true);
const app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
app.use(bodyParser.json());
app.use(express.static('dist'));

global.crawlerinfo = {
  state: 0,
  data: null
};
global.TinexCrawler = new TinexCrawler();
global.PaketCrawler = new PaketCrawler();
global.ReptilCrawler = new ReptilCrawler();


app.get('/api/getcategoriescrawlTinex', (req, res) => {
  global.TinexCrawler.crawlCategoryInfo();
  res.send(global.crawlerinfo);
});




app.get('/api/getcategoriescrawlPaket', (req, res) => {
  global.PaketCrawler.crawlCategoryInfo();
  global.PaketCrawler.crawlSubCategoriesInfo();
  res.send(global.crawlerinfo);
});

app.get('/api/getproductscrawlTinex', (req, res) => {
  global.TinexCrawler.crawlCategoryListings();
  res.send(global.crawlerinfo);
});

app.get('/api/getproductscrawlReptil', (req, res) => {
  global.ReptilCrawler.crawlCategoryListings();
  res.send(global.crawlerinfo);
});

app.get('/api/crawlinglogs/:id',(req,res) =>{
  let log = req.body.data
  dbHelper.getCrawlingLogsBySourceID(req.params.id, function (result) {
    res.send(result)
  })
})

app.post('/api/crawlinglogs',(req,res) =>{
  let log = req.body.data
  dbHelper.insertCrawlingLogs(log.sourceid, log.products, log.categories, log.error, function (result) {
    res.send(result)
  })
})

app.get('/api/getproductscrawlPaket', (req, res) => {
  global.PaketCrawler.crawlCategoryListings();
  res.send(global.crawlerinfo);
});

app.get('/api/getcategoriescrawlReptil', (req, res) => {
  global.ReptilCrawler.crawlCategoryInfo();
  global.ReptilCrawler.crawlSubCategoriesInfo();
  res.send(global.crawlerinfo);
});

app.get('/api/getcrawlinfo', (req, res) => {
  console.log(global.crawlerinfo);
  res.send(global.crawlerinfo);
});

app.get('/api/getProductsByCategoryID/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getProductsByCategoryID(id, function (result) {
    res.send(result);
  });
});


app.get('/api/favorites/:id',(req,res) =>{
  dbHelper.getFavoritesSourceProducts(req.params.id,function (result) {
    res.send(result)
  })
})

app.get('/api/favoritesProducts',(req,res) =>{
  dbHelper.getFavoritesProducts(function (result) {
    res.send(result)
  })
})


app.post('/api/favoritesProducts',(req,res) =>{
  dbHelper.setFavoritesProducts(req.body.id,function (result) {
    res.send(result)
  })
})


app.post('/api/favoritesProductsOf',(req,res) =>{
  console.log()
  dbHelper.setFavoritesProductsOf(req.body.id,function (result) {
    res.send(result)
  })
})

app.get('/api/getCategoriesBySourceID/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getCategoriesBySourceID(id, function (result) {
    res.send(result);
  });
});

app.get('/api/sourceproducts/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getSourceProductDetail(id, function (result) {
    res.send(result);
  });
});

app.get('/api/sourceproductsProductID/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getSourceProductByProductID(id, function (result) {
    res.send(result);
  });
});

app.get('/api/getProductsByCategoryID/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getProductsByCategoryID(id, function (result) {
    res.send(result);
  });
});

app.post('/api/getProductsByIDS/', (req, res) => {
  dbHelper.getProductsByIDS(req.body.ids, function (result) {
    res.send(result);
  });
});

app.get('/api/productsbycategoryid/:id', (req, res) => {
  dbHelper.getFinalProductByCategoryID(req.params.id, function (result) {
    res.send(result);
  });
});

app.delete('/api/sourceproducts/:id', (req, res) => {
  dbHelper.deleteProduct(req.params.id, function (result) {
    res.send(result);
  });
});

app.post('/api/products', (req, res) => {
  dbHelper.insertFinalProduct(req.body.name, req.body.currentCategory.index, function (result) {
    res.send(result);
  });
});

app.get('/api/products/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getFinalProductByID(id, function (result) {
    res.send(result);
  });
});

app.get('/api/sourceproducts', (req, res) => {
  dbHelper.getProducts(function (result) {
    res.send(result);
  });
});


app.put('/api/sourceproductsupdate', (req, res) => {
  console.log(req.body)
  let resultarray = [];
  if (req.body.names) {
    req.body.names.forEach(function (el) {
      dbHelper.deleteSourceProduct(req.body.insertID, el, function (result) {
        resultarray.push(result);
      });
    });
  } else {
    dbHelper.updateSourceProduct(req.body.insertID, req.body.updateID, function (result) {
        resultarray.push(result);
      }
    );
  }
  res.send(resultarray);
});


app.get('/api/sourceproductslogs/:id', (req, res) => {
  dbHelper.getProductLogs(req.params.id, function (result) {
    res.send(result);
  });
});

app.get('/api/crawlablecategories/:id', (req, res) => {
  dbHelper.getCrawlable(req.params.id, function (result) {
    res.send(result);
  });
});

app.get('/api/getcountcrawlable/', (req, res) => {
  dbHelper.getCountCrawlable(1, function (result) {
    res.send(result);
  });
});

app.get('/api/categories', (req, res) => {
  dbHelper.getFinalCategories(function (result) {
    res.send(result);
  });
});

app.put('/api/sourcecategories/:id', (req, res) => {
  let resArray = [];

  dbHelperCrawl.bulk_updateSourceCategoryCrawl(req.body, req.params.id, function (result) {
    resArray.push(result);
    res.send(resArray);
  });
});


app.get('/api/getCategoriesBySourceID/:id', (req, res) => {
  var id = req.params.id;
  dbHelper.getCategoriesBySourceID(id, function (result) {
    res.send(result);
  });
});

app.get('/api/availableproducts/:id', (req, res) => {
  dbHelper.getAvailableSourceProductsByCategory(req.params.id, function (result) {
    res.send(result);
  });
});

app.post('/api/categories', (req, res) => {
  dbHelper.insertCategory(req.body.name, req.body.parent_id, function (result) {
    res.send(result);
  });
});

app.put('/api/categories', (req, res) => {
  dbHelper.updateFinalCategory(req.body.id, req.body.name, function (result) {
    res.send(result);
  });

});

app.put('/api/productdetails',(req,res) =>{
  let data = req.body.data;
  dbHelper.updateProductData(data['id'],data['name'],data['product_id'],data['package'], data['weight'],data['unit_of_measure'], data['producer'],data['product_type'],data['description'], function (result) {
    res.send(result)
  })
})

app.get('/api/startall',(req,res)=>{
  CrawlerObj.startAll()
})

app.get('/api/getproductdetail/:id', (req,res) =>{
  dbHelper.getProductDetail(req.params.id, function(result){
    res.send(result)
  })
})

app.get('/api/getPictureProductDetail/:id', (req,res) =>{
    dbHelper.getPictureDetail(req.params.id, function (result) {
        res.send(result);
    })
})



app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
