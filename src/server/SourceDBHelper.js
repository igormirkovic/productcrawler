const mysql = require('mysql');

class SourceDBHelper {

  constructor(persisConnection = false) {
    this.connectionString = {
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'crawler',
      multipleStatements: true
    };

    this.connection = null;
    this.persistConnection = persisConnection;
  }

  bulk_queryDB(query, data, returnFunc) {
    this.createConnection();

    const fullQuery = data.map(params => this.connection.format(query, params)).join(';');
    console.log('bulk_queryDB');
    console.log(fullQuery);

    this.connection.query(fullQuery, (error, res) => {
      returnFunc(error, res);
    });

    if(!this.persistConnection){
      this.closeConnection();
    }
  }


  queryDB(query, data, returnFunc) {
    this.createConnection();

    if (data != null) {
      const fullQuery = this.connection.format(query, data);
      // console.log(fullQuery);
      this.connection.query(fullQuery, (error, res) => {
        returnFunc(error, res);
      });
    } else {
      this.connection.query(query, (error, res) => {
        returnFunc(error, res);
      });
    }

    if(!this.persistConnection){
      this.closeConnection();
    }
  }

  closeConnection() {
    if(this.connection != null) {
      this.connection.end();
      this.connection = null;
    }
  }

  createConnection() {
    if (this.connection == null) {
      this.connection = mysql.createConnection(this.connectionString);
      this.connection.connect();
    }
  }


  updateOrInsertCategory(name, url, parent_id, source_id, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE url = ?', url, (error, result) => {
      if (error) throw error;
      if (result.length !== 0) {
        this.queryDB('UPDATE sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ? WHERE url = ?',
          [name, url, parent_id, source_id, url], (err, lastID) => {
            if (err) throw err;
            callback(result[0].id);
          });
      } else {
        this.queryDB('INSERT INTO sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ?',
          [name, url, parent_id, source_id], (err, res) => {
            if (err) throw err;
            if(source_id == 1){
              this.insertCategory(name, parent_id,() => {
                console.log('inserting');
              })
            }
            callback(res.insertId);
          });
      }
    });

  }



  updateOrInsertTinexSourceCategory(name, url, parent_id, category_id, source_id, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE url = ?', url, (error, result) => {
      if (error) throw error;
      if (result.length !== 0) {
        this.queryDB('UPDATE sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ? WHERE url = ?',
          [name, url, parent_id, source_id, url], (err, lastID) => {
            if (err) throw err;
            callback(result[0].id);
          });
      } else {
        this.insertCategory(name, category_id,(category_id) => {
          this.queryDB('INSERT INTO sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ?, category_id = ?',
            [name, url, parent_id, source_id, category_id], (err, res) => {
              if (err) throw err;
              callback(res.insertId, category_id);
            });
        })
      }
    });
  }


  getFavoritesSourceProducts(sourceid, callback){
    this.queryDB('SELECT * FROM sourceproducts WHERE favorites = ? and source_id = ?',[1, sourceid], (error, result) =>{
      if (error) throw error;
      callback(result);
    })
  }

  setFavorites(id, callback){
    this.queryDB('UPDATE sourceproducts SET favorites = ? WHERE id = ?',[1, id], (error, result) =>{
      if (error) throw error;
      callback(result);
    })
  }

  setFavoritesOf(id, callback){
    this.queryDB('UPDATE sourceproducts SET favorites = ? WHERE id = ?',[0, id], (error, result) =>{
      if (error) throw error;
      callback(result);
    })
  }

  getFavoritesProducts(callback){
      this.queryDB('select p.*, sp1.id as sp1id, sp1.price as sp1price, sp1.name as sp1name,sp2.id as sp2id, sp2.price as sp2price, sp2.name as sp2name,sp3.id as sp3id, sp3.price as sp3price, sp3.name as sp3name from products p\n'
          + 'left join sourceproducts sp1 on sp1.product_id = p.id and sp1.source_id = 1\n'
          + 'left join sourceproducts sp2 on sp2.product_id = p.id and sp2.source_id = 2\n'
          + 'left join sourceproducts sp3 on sp3.product_id = p.id and sp3.source_id = 3\n'
          + 'where p.favorites = ?',
          [1], (error, result) => {
              if (error) throw error;
              callback(result);
          });

  }

  setFavoritesProducts(id, callback){
    this.queryDB('UPDATE products SET favorites = ? WHERE id = ?',[1, id], (error, result) =>{
      if (error) throw error;
      callback(result);
    })
  }

  updateProductData(id, name, product_id, packages, weight, unit_of_measure, producer, product_type, description, callback){
    this.queryDB('update products set name = ?, product_id = ?, package = ?, weight = ?, unit_of_measure = ?, producer = ?, product_type = ?, description = ? where id = ?',[name, product_id, packages, weight, unit_of_measure, producer, product_type, description,id], (err,res)=>{
      if(err) throw err;
      callback(res)
    })
  }

  setFavoritesProductsOf(id, callback){
    this.queryDB('UPDATE products SET favorites = ? WHERE id = ?',[0, id], (error, result) =>{
      if (error) throw error;
      callback(result);
    })
  }






  getFinalCategories(callback) {
    this.queryDB('SELECT * FROM categories', null, (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  updateFinalCategory(id, name, callback) {
    this.queryDB('UPDATE categories SET name = ? WHERE id = ? ', [name, id], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }


  insertFinalProduct(name, category_id, callback) {
    this.queryDB('INSERT INTO products SET name = ?, category_id = ? ', [name, category_id], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }


  getSourceProductByProductID(id, callback) {
    this.queryDB('SELECT * FROM sourceproducts WHERE product_id = ?', id, (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }

  getSourceProductDetail(id, callback) {
    this.queryDB('SELECT * FROM sourceproducts WHERE id = ?', id, (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }

  updateSourceProduct(product_id, id, callback) {
    this.queryDB('UPDATE sourceproducts SET product_id = ? WHERE id = ?', [product_id, id], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }

  deleteSourceProduct(product_id, name, callback) {
    this.queryDB('UPDATE sourceproducts SET product_id = ? WHERE name = ?', [product_id, name], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }

  getProducts(callback) {
    this.queryDB('SELECT * FROM sourceproducts', null, (err, res) => {
      callback(res);
    });

  }

  getProductLogs(id, callback) {
    this.queryDB('SELECT * FROM sourceproductslogs WHERE sourceproductid = ?', id, (err, result) => {
      if (err) throw err;
      callback(result);
    });

  }

  getChildCategoriesBySourceID(sourceID, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE crawlable = ? and source_id = ?', [1, sourceID], (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  insertCrawlingLogs(sourceID, productsCount, categoriesCount, error, callback){
    this.queryDB('INSERT INTO sourcelog SET date = NOW(), categories = ?, products = ?, error = ?, sourceid = ?',[categoriesCount, productsCount, error, sourceID], (error,result) =>{
      if(error) throw error;
      callback(result);
    })
  }

  getCrawlingLogsBySourceID(sourceID, callback){
    this.queryDB('SELECT * FROM sourcelog where sourceid= ? ',[sourceID],(error, result) =>{
      if(error) throw error;
      callback(result)
    })
  }

  getCategoriesBySourceIDPR(sourceID, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE source_id = ?', [sourceID], (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  getProductsByCategoryIDlvl3(categoryID, callback) {
    this.queryDB('select p.* from sourceproducts p\n'
          + ' inner join sourcecategories cc on cc.id = p.category_id\n'
          + ' inner join sourcecategories cc1 on cc1.id = cc.parent_id\n'
          + ' inner join sourcecategories cc2 on cc2.id = cc1.parent_id\n'
          + ' where cc.id = ? or cc1.id = ? or cc2.id = ?', [categoryID, categoryID, categoryID], (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  getProductsByCategoryID(categoryID, callback) {
    this.queryDB('select p.* from sourceproducts p\n'
      + ' inner join sourcecategories cc on cc.id = p.category_id\n'
      + ' inner join sourcecategories cc1 on cc1.id = cc.parent_id\n'
      + ' where cc.id = ? or cc1.id = ?', [categoryID, categoryID], (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  getAvailableSourceProductsByCategory(categoryID, callback) {
    this.queryDB('SELECT * FROM sourceproducts WHERE category_id = ? AND ISNULL(product_id)', categoryID, (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  deleteProduct(productid, callback) {
    this.queryDB('DELETE FROM products where id = ?', productid, (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  getFinalProductByCategoryID(category_id, callback) {
    this.queryDB('select p.*, sp1.id as sp1id, sp1.price as sp1price, sp1.name as sp1name,sp2.id as sp2id, sp2.price as sp2price, sp2.name as sp2name,sp3.id as sp3id, sp3.price as sp3price, sp3.name as sp3name from products p\n'
      + 'inner join categories cc on cc.id = p.category_id\n'
      + 'inner join categories cp on cp.id = cc.parent_id\n'
      + 'left join sourceproducts sp1 on sp1.product_id = p.id and sp1.source_id = 1\n'
      + 'left join sourceproducts sp2 on sp2.product_id = p.id and sp2.source_id = 2\n'
      + 'left join sourceproducts sp3 on sp3.product_id = p.id and sp3.source_id = 3\n'
      + 'where cp.id = ? or cc.id = ?',
    [category_id, category_id], (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }


  getCategoriesBySourceID(sourceID, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE source_id = ?', sourceID, (error, result) => {
      if (error) throw error;
      callback(result);
    });

  }

  updateOrInsertCategoryByName(name, url, parent_id, source_id, callback) {
    if (url !== null) {
      this.queryDB('SELECT * FROM sourcecategories WHERE name = ? AND url = ?', [name, url], (error, result) => {
        if (error) throw error;
        if (result.length !== 0) {
          this.queryDB('UPDATE sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ? WHERE name = ? AND url = ?',
            [name, url, parent_id, source_id, name, url], (err, lastID) => {
              if (err) throw err;
              callback(result[0].id);
            });
        } else {
          this.queryDB('INSERT INTO sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ?',
            [name, url, parent_id, source_id], (err, res) => {
              if (err) throw err;
              callback(res.insertId);
            });
        }
      });
    } else {
      this.queryDB('SELECT * FROM sourcecategories WHERE name = ? AND ISNULL(url)', [name, url], (error, result) => {
        if (name === 'Пекара') {
          console.log('aaa');
        }
        if (error) throw error;
        if (result.length !== 0) {
          this.queryDB('UPDATE sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ? WHERE name = ? AND ISNULL(url)',
            [name, url, parent_id, source_id, name], (err, lastID) => {
              if (err) throw err;
              callback(result[0].id);
            });
        } else {
          this.queryDB('INSERT INTO sourcecategories SET name = ?, url = ?, parent_id = ?, source_id = ?',
            [name, url, parent_id, source_id], (err, res) => {
              if (err) throw err;

              callback(res.insertId);
            });
        }
      });
    }

  }

  insertCategory(name, category_id, callback) {
      this.queryDB('INSERT INTO categories SET name = ?, parent_id = ?', [name, category_id], (err, res) => {
        if (err) throw err;
        callback(res.insertId);
      });
    };
  updateCategory(id, name, parent_id, callback){
    this.queryDB('UPDATE categories SET name = ?, parent_id = ? WHERE id = ?',[name, parent_id,id],(err,res)=>{
      if(err) throw err;
      callback(id);
    })
  }

  getCrawlable(sourceid, callback) {
    this.queryDB('SELECT * FROM sourcecategories WHERE source_id = ? AND crawlable = ?', [sourceid, 1], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }


  insertSourceProductLog(name, url, category_id, description, image_url, price, source_id, sku, sourceproduct_id) {
    this.queryDB('INSERT INTO sourceproductslogs SET name = ?, url = ?, category_id = ?, description = ? ,image_url = ?, price = ? , sku = ? ,source_id = ?, sourceproductid = ?, crawl_date = NOW()',
      [name, url, category_id, description, image_url, price, sku, source_id, sourceproduct_id], (err, result) => {
        if (err) throw err;
      });

  }

  getCountCrawlable(num,callback) {
    this.queryDB('select sum(crawlable) as crawlableCount from sourcecategories group by source_id', [], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }

  makeAllUncrawlable(sourceid, callback) {
    this.queryDB('UPDATE sourcecategories SET crawlable = ? WHERE source_id = ? AND crawlable = ?', [0, sourceid, 1], (err, res) => {
      if (err) throw err;
      callback(res);
    });

  }


  getProductsByIDS(ids, callback){
    console.log(ids);
    this.queryDB('select * from sourceproducts where category_id in (?)',[ids], (err,res) =>{
      if(err) throw err;
      callback(res);
    });

  }

  insertProducts(id, name, category_id, callback){
    this.queryDB('INSERT INTO products SET name = ?, id = ?, category_id = ?',[name, id, category_id], (err, res) =>{
      if(err) throw err;
      callback()
    })
  }

  updateProducts(id, name, category_id, callback){
    this.queryDB('UPDATE products SET name = ?, category_id = ? WHERE id = ?',[name,category_id,id], (err, res) =>{
      if(err) throw err;
      callback()
    })
  }


  updateOrInsertProduct(name, url, category_id, description, image_url, price, source_id, sku, callback) {
    this.queryDB('SELECT * FROM sourceproducts WHERE url = ?', [url], (error, result) => {
      if (isNaN(price)) {
        price = 0;
      }
      if (error) throw error;
      if (result.length !== 0) {
        this.queryDB('UPDATE sourceproducts SET name = ?, url = ?, category_id = ?, description = ?, image_url = ?, price = ? , sku = ?, source_id = ? WHERE url = ?',
          [name, url, category_id, description, image_url, price, sku, source_id, url], (err, lastID) => {
            if (err) throw err;
            callback(result[0].id);
            this.insertSourceProductLog(name, url, category_id, description, image_url, price, source_id, sku, result[0].id);
          });
      } else {
        this.queryDB('INSERT INTO sourceproducts SET name = ?, url = ?, category_id = ?, description = ? ,image_url = ?, price = ? , sku = ? ,source_id = ?',
          [name, url, category_id, description, image_url, price, sku, source_id], (err, res) => {
            if (err) throw err;
            callback(res.insertId);
            this.insertSourceProductLog(name, url, category_id, description, image_url, price, source_id, sku, res.insertId);
          });
      }
    });

  }

  updateOrInsertProductTinex(name, url, category_id, finalcategory_id,  description, image_url, price, source_id, sku, callback) {
    this.queryDB('SELECT * FROM sourceproducts WHERE url = ?', [url], (error, result) => {
      if (isNaN(price)) {
        price = 0;
      }
      if (error) throw error;
      if (result.length !== 0) {
        this.queryDB('UPDATE sourceproducts SET name = ?, url = ?, category_id = ?, description = ?, image_url = ?, price = ? , sku = ?, source_id = ? WHERE url = ?',
          [name, url, category_id, description, image_url, price, sku, source_id, url], (err, lastID) => {
            if (err) throw err;
            callback(result[0].id);
            this.insertSourceProductLog(name, url, category_id, description, image_url, price, source_id, sku, result[0].id);
          });
      } else {
        this.insertProductsTinex(name, finalcategory_id, (product_id) =>{
          this.queryDB('INSERT INTO sourceproducts SET name = ?, url = ?, category_id = ?, description = ? ,image_url = ?, price = ? , sku = ? ,source_id = ?, product_id = ?',
            [name, url, category_id, description, image_url, price, sku, source_id, product_id], (err, res) => {
              if (err) throw err;
                callback(res.insertId);
                this.insertSourceProductLog(name, url, category_id, description, image_url, price, source_id, sku, res.insertId);
              })
            });

      }
    });
  }

  insertProductsTinex(name, category_id, callback){
    this.queryDB('INSERT INTO products SET name = ?, category_id = ?',[name, category_id], (err, res) =>{
      callback(res.insertId)
    })
  }

  updateSourceCategoryCrawl(category, source_id, callback) {
    this.queryDB('update sourcecategories set crawlable = ? where id = ? and source_id = ?',[category.crawlableNow, category.id, source_id], (error,result) =>{
      if(error) throw error;
      callback(result);
    })
  }


  bulk_updateSourceCategoryCrawl(categories, source_id, callback) {
    const data = categories.map(category => [category.crawlableNow, category.id, source_id]);

    this.bulk_queryDB('update sourcecategories set crawlable = ? where id = ? and source_id = ?',data, (error,result) =>{
      if(error) throw error;
      callback(result);
    })
  }

  insertProductCategories(product_id, category_id, callback) {
    this.queryDB('SELECT * FROM sourceproductcategories WHERE product_id = ? AND category_id = ?', [product_id, category_id], (error, result) => {
      if (error) throw error;
      if (result.length === 0) {
        console.log('im here');
        this.queryDB('INSERT INTO sourceproductcategories SET product_id = ?, category_id = ?', [product_id, category_id], (error, res) => {
          if (error) throw error;
          callback(res);
        });
      }
    });

  }

  getCategories(source_id, callBack) {
    this.queryDB('SELECT * FROM sourcecategories WHERE source_id = ?', source_id, (error, res) => {
      callBack(res);
    });

  }


  getCategoryByURL(url, callBack) {
    this.queryDB('SELECT * FROM sourcecategories WHERE url = ?', url, (error, res) => {
      if (error) throw error;
      console.log(res);
      callBack(res);
    });
  }

  updateProductsByID(id, image_url, sku, description, callBack) {
    this.queryDB('UPDATE sourceproducts SET image_url = ?, sku = ?, description = ? WHERE id = ?', [image_url, sku, description, id], (error, res) => {
      if (error) throw error;
      callBack(res);
    });

  }

  getProductByID(id, callBack) {
    this.queryDB('SELECT * FROM sourceproducts WHERE id = ?', id, (error, res) => {
      if (error) throw error;
      callBack(res);
    });

  }

  getFinalProductByID(id, callBack) {
    this.queryDB('SELECT * FROM products WHERE id = ?', id, (error, res) => {
      if (error) throw error;
      callBack(res);
    });

  }


  getProductDetail(id, callback){
    this.queryDB('select * from products where id = ?',[id], (error, res) =>{
      if(error) throw error;
      callback(res)
    })
  }


  getPictureDetail(id, callback){
      this.queryDB('select image_url from sourceproducts where source_id = 1 and product_id = ?',[id], (error, res) =>{
          if(error) throw error;
          callback(res)
      })
  }

}

module.exports = SourceDBHelper;
