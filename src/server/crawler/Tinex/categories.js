var Categories = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Categories({
    maxConnections: 10,
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;
            console.log($("title").text());
        }
        done();
    }
});

const startCrawler = () => {
    c.queue(
        [
            {
                uri: 'https://www.e-tinex.mk/',
                jQuery: true,
                callback: getCategories
            }
        ]
    );
};

const getCategories = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        $('.tree_menu .mtree > li > ul > li > a[href]').each((i, el) => {
            let catUrl = $(el).attr('href');
            dbHelper.updateOrInsertTinexSourceCategory($(el).text(), catUrl, null, null, 1, function (parentID, category_id) {
                $(el.parent).find('ul > li > a[href]').each((i, e) => {
                    let catUrl = $(e).attr('href');
                    dbHelper.updateOrInsertTinexSourceCategory($(e).text(), catUrl, parentID, category_id, 1, function () {});
                })
            });
        })
    }
};

startCrawler();

