var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

let url = 'https://www.e-tinex.mk/';



var c = new Crawler({
    maxConnections : 10,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});

let currentProduct = null;

const startCrawler = () => {
    c.queue(
        [
            {
                uri: url + currentProduct.url,
                jQuery: true,
                callback: getProductsDetail
            }
        ]
    );
};


const getProductsDetail = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        let sku_text = $('.right_side_wide_block .kategorii_pocetna .block_2 .cena_akcija_proizvod p').text();
        let n = sku_text.split(" ");

        dbHelper.updateProductsByID(currentProduct.id, $('.right_side_wide_block .kategorii_pocetna .slika_proiz_stick a').attr('href'), n[n.length-1], $('.right_side_wide_block .kategorii_pocetna .block_2 #ctl00_ContentPlaceHolder1_pnlDesc .opis_proizvod').text().trim(), function () {} );

        $('.right_side_wide_block .kategorii_pocetna .line_pateka a').each((i,el) =>{
            dbHelper.getCategoryByURL($(el).attr('href'), function (category) {
                if(category.length === 0){
                    console.log('cat not found');
                }else{
                    console.log('cat found');
                    dbHelper.insertProductCategories(currentProduct.id, category[0].id, function (res) {
                        console.log(res);
                    })
                }
            })


        });



            console.log("finished");
    }


};

dbHelper.getProductByID(15, function (products) {
    if(products.length === 0){
        console.log('product not found');
    }else{
        currentProduct = products[0];
        startCrawler();
    }
});







