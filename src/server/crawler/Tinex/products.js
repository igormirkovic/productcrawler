var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Crawler({
    maxConnections : 1000,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});

let numberProducts = 0;
let currentCategory = null;
let pageNumber = 1;
let url = 'https://www.e-tinex.mk/';


const startCrawler = () => {
    let wholeUrl = url + currentCategory.url + '&p=' + pageNumber + '&i=96';
    console.log(wholeUrl);
    c.queue(
        [
            {
                uri: wholeUrl,
                jQuery: true,
                callback: getProducts
            }
        ]
    );

};


const getProducts = (error, res) =>{
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        $('.right_side_wide_block .kategorii_pocetna .block_6 .grid_category1').each((i,el) =>{
            numberProducts = i;
            dbHelper.updateOrInsertProduct($(el).find('a > div > div').text(),$(el).find('a').attr('href'),currentCategory.id, null, null, parseFloat($(el).find('div.price_cont').text().trim().replace(/^\D+/g, '')), 1, null, function () {})
});
        if(numberProducts === 95){
            pageNumber++;
            numberProducts = 0;
            startCrawler();
        }
        else{
            goNext();
            console.log('finished');
        }
    }
};


let allCategories = [];
let catIndex = 0;


goNext = () => {
    if (allCategories.length > catIndex) {
        currentCategory = allCategories[catIndex];
        catIndex++;
        pageNumber = 1;
        startCrawler();

    }
    else{
        console.log('finished_page');
    }
};


getAll = () => {
    dbHelper.getCategories(1, function (res) {
        allCategories = res;
        goNext();
    })
};

// Category.findById("5cdac3e1794e4c36702046d0", function (err, category) {
//     console.log(category);
//     currentCategory = category;
//     if(category == null){
//         console.log("cat not found");
//         return;
//     }
//     startCrawler();
// });







getAll();



