const Crawler = require('crawler');
const SourceDBHelper = require('../../SourceDBHelper');




class TinexCrawler {


  constructor(callback = null) {
    this.url = 'https://www.e-tinex.mk/';
    this.sourceid = 1;
    this.dbHelper = new SourceDBHelper(true);
    this.crawler = new Crawler({
      maxConnections: 10000,
      rateLimit: 500
    });


    this.crawler.on('drain', () => {
      this.crawlerFinished();
      if(callback){
        callback()
      }
    });
  }


  crawlerFinished() {
    console.log('drain');
    global.crawlerinfo.state = 0;
    this.dbHelper.closeConnection();
  }

  crawlCategoryInfo() {
    this.crawler.queue(
      [
        {
          uri: this.url,
          jQuery: true,
          callback: (error, res, done) => {
            this.scrapCategories(error, res, done);
            done();
          }
        }
      ]
    );
    global.crawlerinfo.data = {
      sourceid: 1,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize,
      error: ''
    };
    global.crawlerinfo.state = 1;
  }

  scrapCategories(error, res, done) {
    console.log('getCategories callback');
    if (error) {
      console.error(error);
      global.crawlerinfo.data.error += error;
    } else {
      const { $ } = res;
      $('.tree_menu .mtree > li > ul > li > a[href]')
        .each((i, el) => {
          global.crawlerinfo.data.categories++;
          const catUrl = $(el)
            .attr('href');
          this.dbHelper.updateOrInsertTinexSourceCategory($(el)
            .text(), catUrl, null, null, 1,(parentID, category_id) => {
            $(el.parent)
              .find('ul > li > a[href]')
              .each((i, e) => {
                const catUrl = $(e)
                  .attr('href');
                this.dbHelper.updateOrInsertTinexSourceCategory($(e)
                  .text(), catUrl, parentID, category_id, 1, () => {
                  global.crawlerinfo.data.categories++;
                });
              });
            // this.dbHelper.updateParentCategory(parentID);
          });
        });
    }
  }

  getCategoryListingFullURL(categoryURL, page) {
    if(categoryURL == null){

    }
    return `${this.url + categoryURL}&p=${page}&i=96`;
  }

  crawlCategoryListings() {
    global.crawlerinfo.data = {
      sourceid: 1,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize
    };
    global.crawlerinfo.state = 1;
    this.dbHelper.getChildCategoriesBySourceID(1, (result) => {
      result.forEach((category) => {
        global.crawlerinfo.data.categories++;
        this.queueCategoryListing(category, 1);
      });
    });

  }



  queueCategoryListing(category, page) {
    console.log(`${category.name} - ${page}`);
    this.crawler.queue(
      [
        {
          uri: `${this.url + category.url}&p=${page}&i=96`,
          jQuery: true,
          category,
          page,
          callback: (error, res, done) => {
            this.scrapCategoryListing(error, res, done);
            done();
          }
        }
      ]
    );
  }

  scrapCategoryListing(error, res) {

    const nextPage = res.options.page + 1;
    const { category } = res.options;
    console.log(res.options);
    let productCount = 0;
    if (error) {
      console.error(error);
      global.crawlerinfo.data.error += error;
    } else {
      const { $ } = res;
      $('.right_side_wide_block .kategorii_pocetna .block_6 .grid_category1')
        .each((i, el) => {
          global.crawlerinfo.data.products++;
          global.crawlerinfo.data.range = this.crawler.queueSize;
          productCount = i;
          this.dbHelper.updateOrInsertProductTinex($(el)
            .find('a > div > div')
            .text(), $(el)
            .find('a')
            .attr('href'), category.id,  category.category_id, null, $(el).find('img').attr('src'), parseFloat($(el)
            .find('div.price_cont')
            .text()
            .trim()
            .replace(/^\D+/g, '')), 1, null, () => {
          });
        });
      this.dbHelper.closeConnection();
      if (productCount === 95) {
        this.queueCategoryListing(category, nextPage);
      }
    }
  }
}
module.exports = TinexCrawler;


