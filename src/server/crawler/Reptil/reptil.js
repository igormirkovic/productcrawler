const Crawler = require('crawler');
const SourceDBHelper = require('../../SourceDBHelper');

class ReptilCrawler {
  constructor() {
    this.url = 'http://www.marketonline.mk/';
    this.sourceid = 3;
    this.dbHelper = new SourceDBHelper(true);
    this.crawler = new Crawler({
      maxConnections: 1000,
      rateLimit: 3000
    });

    this.listingsCrawled = 0;
    this.productsScraped = 0;


    this.crawler.on('drain', () => {
      this.crawlerFinished();
    });
  }

  crawlerFinished() {
    console.log('drain');
    global.crawlerinfo.state = 0;
    this.dbHelper.closeConnection();
  }

  scrapCategoryListing(error, res, done) {
    const { category } = res.options;
    const { page } = res.options;

    this.listingsCrawled++;

    if (error) {
      console.error(error);
      global.crawlerinfo.data.error+= error;
    } else {
      const { $ } = res;
      $('.row .product-list-item').each((i, el) => {
        global.crawlerinfo.data.products++;
        global.crawlerinfo.data.range = this.crawler.queueSize;
        this.dbHelper.updateOrInsertProduct($(el).find('img').attr('title'),
          $(el).find('.product-thumbnail').attr('href'),
          category.id,
          null,
          $(el).find('.product-thumbnail > img').attr('src')
          , parseFloat($(el).find('>.thumbnail > .caption .priceCurrent').text().slice(3).trim())
          ,this.sourceid,
          null,
          () => {});


        this.productsScraped++;

        if (i === 19) {
          this.queueCategoryListing(category, page + 1);
        }
      });

      console.log(`Listings ${this.listingsCrawled} Products ${this.productsScraped} Crawler size ${this.crawler.queueSize}`);
    }
  }

  queueCategoryListing(category, page) {
    console.log(`${category.name} - ${page}`);
    this.crawler.queue(
      [
        {
          uri: `${category.url}?page=${page}/`,
          jQuery: true,
          category,
          page,
          callback: (error, res, done) => {
            this.scrapCategoryListing(error, res, done);
            done();
          }
        }
      ]
    );
  }

  crawlCategoryInfo() {
    this.crawler.queue(
      [
        {
          uri: this.url,
          jQuery: true,
          callback: (error, res, done) => {
            this.scrapCategories(error, res, done);
            done();
          }
        }
      ]
    );
    global.crawlerinfo.data = {
      sourceid: 3,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize,
      error: ''
    };
    global.crawlerinfo.state = 1;
  }

  scrapCategories(error, res, done) {
    if (error) {
      console.error(error);
      global.crawlerinfo.data.error += error;
    } else {
      const { $ } = res;
      $('.navbar-header > .hidden-xs > #Store_Navigation > .nscMenuItemLevel-1')
        .each((i, el) => {
          this.dbHelper.updateOrInsertCategoryByName($(el)
            .find('> span > span')
            .text()
            .trim(), null, null, this.sourceid, (catID) => {
            // this.dbHelper.updateParentCategory(catID);
            $(el)
              .find('> .nscMenuContainerLevel-2 > .nscMenuItemID-NONLINKTITLE')
              .each((i, e) => {
                this.dbHelper.updateOrInsertCategoryByName($(e)
                  .find('> span > span')
                  .text()
                  .trim(), null, catID, this.sourceid, (subCatID) => {
                  // this.dbHelper.updateParentCategory(subCatID);

                  $(e)
                    .find('> .nscMenuContainerLevel-3 > li a')
                    .each((i, e2) => {
                      this.dbHelper.updateOrInsertCategoryByName($(e2)
                        .find('span')
                        .text()
                        .trim(), $(e2)
                        .attr('href'), subCatID, this.sourceid, () => {

                      });
                    });
                });
              });
            $(el)
              .find('> .nscMenuContainerLevel-2 > li  a')
              .each((i, e) => {
                this.dbHelper.updateOrInsertCategoryByName($(e)
                  .find('span')
                  .text()
                  .trim(), $(e)
                  .attr('href'), catID, this.sourceid, () => {
                });
              });
          });
        });

      console.log('finished');
    }
  }

    crawlCategoryListings(callback) {
      global.crawlerinfo.data = {
        sourceid: 3,
        products: 0,
        categories: 0,
        range: this.crawler.queueSize,
        error: ''
      };
      global.crawlerinfo.state = 1;
      this.dbHelper.getChildCategoriesBySourceID(this.sourceid, (result) => {
        result.forEach((category) => {
          global.crawlerinfo.data.categories++;
          this.queueCategoryListing(category, 1);
        });
      });
      if(callback !== null){
        console.log('finished all three')
      }
  }

  crawlSubCategoriesInfo() {
    global.crawlerinfo.data = {
      sourceid: 3,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize,
      error: ''
    };
    global.crawlerinfo.state = 1;
    this.dbHelper.getChildCategoriesBySourceID(this.sourceid, (result) => {
      result.forEach((category) => {
        global.crawlerinfo.data.categories++;
        this.crawler.queue(
          [
            {
              uri: category.url,
              jQuery: true,
              category,
              callback: (error, res, done) => {
                this.scrapSubCategories(error, res, done);
                done();
              }
            }
          ]
        );
      });
    });
  }


  scrapSubCategories(error, res, done) {
    const { category } = res.options;
    if (error) {
      global.crawlerinfo.data.error += error;
      console.error(error);
    } else {
      const { $ } = res;
      $('.category-pictures-panel a')
        .each((i, el) => {
          global.crawlerinfo.data.categories++;
          this.dbHelper.updateOrInsertCategory($(el)
            .find('h5')
            .text()
            .trim(), $(el)
            .attr('href'), category.id, this.sourceid, () => {
          });
          // this.dbHelper.updateParentCategory(category.id);
        });
    }
  }
}

module.exports = ReptilCrawler;
