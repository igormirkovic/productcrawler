var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();


var c = new Crawler({
    maxConnections : 10,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});


let currentProduct = null;

const startCrawler = () => {
    c.queue(
        [
            {
                uri: currentProduct.url,
                jQuery: true,
                callback: getProductsDetail
            }
        ]
    );
};


const getProductsDetail = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;

        var array = $('.text-product-desc').map(function(){
            return ($(this).text());
        }).get();

        dbHelper.updateProductsByID(currentProduct.id, $('.image').attr('src'), array[1].substr(7), array[0].trim(), function () {}  );

        $('.breadcrumb > li').each((i,el) =>{
            if($(el).find(' > a').attr('href') !== undefined) {
                dbHelper.getCategoryByURL($(el).find(' > a').attr('href'), function (category) {
                    if (category.length === 0) {
                        console.log('cat not found');
                    } else {
                        dbHelper.insertProductCategories(currentProduct.id, category[0].id, function () {
                        })
                    }
                });
            }

            // Category.findOne({url: $(el).attr('href')}, function (err, category) {
            //     if(category == null){
            //         console.log('cat not found');
            //     }else{
            //         if(currentProduct.categories.indexOf(category._id) <= -1){
            //             currentProduct.categories.push(category._id);
            //
            //         }
            //         console.log('cat found');
            //         console.log(currentProduct);
            //     }
            //     currentProduct.save(function (err) {
            //         if(err) console.error(err);
            //     });
            //
            // });

        });


        console.log(currentProduct);

        console.log("finished");
    }


};


dbHelper.getProductByID(981, function (products) {
    if(products.length === 0){
        console.log('product not found');
    }else{
        currentProduct = products[0];
        console.log(currentProduct.url);
        startCrawler();
    }
});





