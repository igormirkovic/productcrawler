var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Crawler({
    maxConnections : 100,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;
            console.log($("title").text());
        }
        done();
    }
});

let currentCategory = null;
let pageNumber = 1;
let numberProducts = 0;

const startCrawler = () => {
    c.queue(
        [
            {
                uri: currentCategory.url + "?page="+pageNumber,
                jQuery: true,
                callback: getProducts
            }
        ]
    );

};


const getProducts = (error, res) =>{
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        $('.category-pictures-panel a').each((i,el) =>{
            dbHelper.updateOrInsertCategory($(el).find('h5').text().trim(), $(el).attr('href'), currentCategory.id, 3, function () {})
});
        $('.row .product-list-item').each((i,el) =>{
            numberProducts = i;
            dbHelper.updateOrInsertProduct($(el).find('> .thumbnail > .caption a').text(), $(el).find('>.thumbnail > a').attr('href'), currentCategory.id, null, $(el).find('>.thumbnail > a > img').attr('src'), parseFloat($(el).find('.priceCurrent').text().substring(4)), 3, null, function () {});
        });
         if(numberProducts === 19){
             pageNumber++;
             numberProducts = 0;
             console.log('next - page');
             startCrawler();
         }
         else{
             goNext();
            console.log('finished');
         }
    }
};


let allCategories = [];
let catIndex = 0;

function goNext(){
    if (allCategories.length > catIndex) {
        currentCategory = allCategories[catIndex];
        catIndex++;
        if(currentCategory.url !== null){
            startCrawler();
        }else{
            catIndex++;
            goNext();
        }
    }
    else{
        console.log('finish');
    }
}
getAll = () => {
    dbHelper.getCategories(3, function (res) {
        allCategories = res;
        goNext();
    })

};

// dbHelper.getCategoriesByID(3, 280, function (category) {
//     if(category.length === 0){
//         console.log('category not found');
//     }else{
//         currentCategory = category[0];
//         startCrawler();
//     }
// });


getAll();



