var Categories = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Categories({
    maxConnections : 10,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});
//
const startCrawler = () => {

    c.queue(
        [
            {
                uri: 'http://www.marketonline.mk/',
                jQuery: true,
                callback: getCategories
            }
        ]
    );

};

const getCategories = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        $('.navbar-header > .hidden-xs > #Store_Navigation > .nscMenuItemLevel-1').each((i, el) => {
            dbHelper.updateOrInsertCategoryByName($(el).find('> span > span').text().trim(), null, null, 3, function (catID) {
                $(el).find('> .nscMenuContainerLevel-2 > .nscMenuItemID-NONLINKTITLE').each((i, e) => {
                    dbHelper.updateOrInsertCategoryByName($(e).find('> span > span').text().trim(), null, catID, 3, function () {
                    })
                });
                $(el).find('> .nscMenuContainerLevel-2 > li  a').each((i, e) => {
                    dbHelper.updateOrInsertCategoryByName($(e).find('span').text().trim(), $(e).attr("href"), catID, 3, function () {
                    })
                });
            });
        });

        console.log('finished');
    }
};



startCrawler();

