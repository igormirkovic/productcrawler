const Crawler = require('crawler');
const SourceDBHelper = require('../../SourceDBHelper');

class PaketCrawler {
  constructor(callback = null) {
    this.url = 'https://www.paket.mk/';
    this.sourceid = 2;
    this.dbHelper = new SourceDBHelper(true);
    this.crawler = new Crawler({
      maxConnections: 1,
      rateLimit: 500
    });

    this.listingsCrawled = 0;
    this.productsScraped = 0;

    this.crawler.on('drain', () => {
      this.crawlerFinished();
      if(callback){
        callback()
      }
    });
  }

  crawlerFinished() {
    console.log('drain');
    global.crawlerinfo.state = 0;
    this.dbHelper.closeConnection();
  }

  crawlCategoryInfo() {
    this.crawler.queue(
      [
        {
          uri: this.url,
          jQuery: true,
          callback: (error, res, done) => {
            this.scrapCategories(error, res, done);
            done();
          }
        }
      ]
    );
    global.crawlerinfo.data = {
      sourceid: 2,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize,
      error: ''
    };
    global.crawlerinfo.state = 1;
  }

  scrapCategories(error, res, done) {
    if (error) {
      global.crawlerinfo.data.error += error;
      console.error(error);
    } else {
      const { $ } = res;
      $('.header .header-ubermenu-nav #ubermenu-main-24-primary .ubermenu-nav > .ubermenu-item:nth-child(-n + 2)')
        .each((i, el) => {
          global.crawlerinfo.data.categories++;
          const catUrl = $(el)
            .find('a')
            .attr('href');
          this.dbHelper.updateOrInsertCategory($(el)
            .find('> a > span')
            .text(), catUrl, null, this.sourceid, (catID) => {
            $(el)
              .find('> ul > li ')
              .each((i, e) => {
                const childCatUrl = $(e)
                  .find('a')
                  .attr('href');
                if (childCatUrl) {
                  this.dbHelper.updateOrInsertCategory($(e)
                    .find('a > span')
                    .text(), childCatUrl, catID, this.sourceid, () => {
                    global.crawlerinfo.data.categories++;
                  });
                }
              });
          });

          console.log('finished');
        });
    }
  }

  crawlSubCategoriesInfo() {
    this.dbHelper.getCategoriesBySourceIDPR(this.sourceid, (result) => {
      result.forEach((category) => {
        global.crawlerinfo.data.categories++;
        this.crawler.queue(
          [
            {
              uri: category.url,
              jQuery: true,
              category,
              callback: (error, res, done) => {
                this.scrapSubCategories(error, res, done);
                done();
              }
            }
          ]
        );
      });
    });
  }

  scrapSubCategories(error, res, done) {
    const { category } = res.options;

    if (error) {
      console.error(error);
      global.crawlerinfo.data.error += error;
    } else {
      const { $ } = res;
      let childFound = false;
      $('.shop-container .wooc_sclist > .category')
        .each((i, el) => {
          global.crawlerinfo.data.categories++;
          childFound = true;
          this.dbHelper.updateOrInsertCategory($(el)
            .find('a > div')
            .text(), $(el)
            .find('a')
            .attr('href'), category.id, this.sourceid, () => {
          });
        });

      // if (childFound) {
      //   this.dbHelper.updateParentCategory(category.id);
      // }
    }
  }


  crawlCategoryListings() {
    global.crawlerinfo.data = {
      sourceid: 2,
      products: 0,
      categories: 0,
      range: this.crawler.queueSize
    };
    global.crawlerinfo.state = 1;
    this.dbHelper.getChildCategoriesBySourceID(this.sourceid, (result) => {
      result.forEach((category) => {
        global.crawlerinfo.data.categories++;
        console.log(category);
        this.queueCategoryListing(category, 1);
      });
    });

  }

  queueCategoryListing(category, page) {
    console.log(`${category.name} - ${page}`);
    this.crawler.queue(
      [
        {
          uri: `${category.url}page/${page}/`,
          jQuery: true,
          category,
          page,
          callback: (error, res, done) => {
            this.scrapCategoryListing(error, res, done);
            done();
          }
        }
      ]
    );
  }

  scrapCategoryListing(error, res, done) {
    const { category } = res.options;
    const { page } = res.options;

    this.listingsCrawled++;

    if (error) {
      global.crawlerinfo.data.error += error;
      console.error(error);
    } else {
      const { $ } = res;
      $('.shop-container .products .box ').each((i, el) => {
        global.crawlerinfo.data.products++;
        global.crawlerinfo.data.range = this.crawler.queueSize;
          let price = $(el).find('.price:nth-child(1) .amount').text().split(',')[0];
         if(price.indexOf('.') > -1 || price.indexOf(',') > -1 ){
          let regex = /[.,\s]/g;
          price = price.replace(regex, '');
        }
        if(isNaN(price)){

        }
          this.dbHelper.updateOrInsertProduct($(el).find('.box-text-products .name > a').text(), $(el).find('.image-none > a').attr('href'), category.id, null, $(el).find('.image-none .container-image-and-badge img').attr('src'), parseInt(price) , this.sourceid, null, () => {});

        this.productsScraped++;

        if (i === 23) {
          this.queueCategoryListing(category, page + 1);
        }
      });

      console.log(`Listings ${this.listingsCrawled} Products ${this.productsScraped} Crawler size ${this.crawler.queueSize}`);
    }
  }
}

module.exports = PaketCrawler;
