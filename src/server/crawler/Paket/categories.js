var Categories = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Categories({
    maxConnections : 10,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});
//
const startCrawler = () => {

    c.queue(
        [
            {
                uri: 'https://www.paket.mk/',
                jQuery: true,
                callback: getCategories
            }
        ]
    );

};


const getCategories = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
            $('.header .header-ubermenu-nav #ubermenu-main-24-primary #ubermenu-nav-main-24-primary > .ubermenu-item').each((i, el) => {
            if(i === 2){
                return;
            }
            let catUrl = $(el).find('a').attr('href');
            dbHelper.updateOrInsertCategory($(el).find('> a > span').text(),catUrl, null, 2, function (catID) {
                $(el).find('> ul > li ').each((i,e) =>{
                    let catUrl = $(e).find('a').attr('href');
                    if(catUrl) {
                        dbHelper.updateOrInsertCategory($(e).find('a > span').text(),catUrl,catID,2,function () {})
                    }
                })
            });
            console.log('finished');
        })


    }
};
startCrawler();

