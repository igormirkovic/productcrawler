var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Crawler({
    maxConnections : 100,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});

let currentCategory = null;
let pageNumber = 1;
let numberProducts = 0;

const startCrawler = () => {
    console.log(currentCategory);
    c.queue(
        [
            {
                uri: currentCategory.url +"page/"+pageNumber+"/",
                jQuery: true,
                callback: getProducts
            }
        ]
    );

};

const getProducts = (error, res) =>{
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
        $('.shop-container .wooc_sclist > .category').each((i,el) =>{
            dbHelper.updateOrInsertCategory($(el).find('a > div').text(), $(el).find('a').attr('href'), currentCategory.id, 2, function () {} );
            });

        $('.shop-container .products > .product-small').each((i,el) =>{
            numberProducts = i;
            dbHelper.updateOrInsertProduct($(el).find('> .col-inner > span').attr('data-gtm4wp_product_name'),$(el).find(' > .col-inner > span').attr('data-gtm4wp_product_url'),currentCategory.id, null,$(el).find('> .col-inner > .product-small > .box-image > .image-none > a > .container-image-and-badge > img').attr('src'),parseFloat($(el).find('> .col-inner > span').attr('data-gtm4wp_product_price')), 2, null, function () {})
        });

         if(numberProducts === 23){
             pageNumber++;
             numberProducts = 0;
             console.log('next - page');
             startCrawler();
         }
         else{
             goNext();
            console.log('finished');
         }
    }
};

// goNext = () => {
//     if (allCategories.length > catIndex) {
//         currentCategory = allCategories[catIndex];
//         console.log(catIndex);
//         console.log(currentCategory.url);
//         catIndex++;
//         startCrawler();
//     }
//     else{
//         console.log('finish');
//     }
// };
//
// getAll = () => {
//     Category.find.where('parentCategory').equals()
//         .exec(function (err,categories) {
//             categories.reverse();
//             allCategories = categories;
//             goNext();
//         })
//
// };

// Category.findById("5cdbfacdf93cbe51acfde866", function (err, category) {
//     currentCategory = category;
//     console.log(currentCategory.url);
//     if(category == null){
//         console.log("cat not found");
//         return;
//     }
//     startCrawler();
// });

let allCategories = [];
let catIndex = 0;


goNext = () => {
    if (allCategories.length > catIndex) {
        currentCategory = allCategories[catIndex];
        catIndex++;
        pageNumber = 1;
        startCrawler();

    }
    else{
        console.log('finished_page');
    }
};


getAll = () => {
    dbHelper.getCategories(2, function (res) {
        allCategories = res;
        goNext();
    })
};

// dbHelper.getCategoriesByID(2, 510, function (category) {
//     if(category === null){
//         console.log('cat not found');
//     }else{
//         currentCategory = category[0];
//         startCrawler();
//     }
// });



getAll();


