var Crawler = require("crawler");
var SourceDBHelper = require('../../SourceDBHelper');
var dbHelper = new SourceDBHelper();

var c = new Crawler({
    maxConnections : 10,
    callback : function (error, res, done) {
        if(error){
            console.log(error);
        }else{
            var $ = res.$;

            console.log($("title").text());
        }
        done();
    }
});
//

let currentProduct = null;

const startCrawler = () => {
    c.queue(
        [
            {
                uri: currentProduct.url,
                jQuery: true,
                callback: getProductsDetail
            }
        ]
    );
};


const getProductsDetail = (error, res) => {
    if (error) {
        console.error(error);
    } else {
        const $ = res.$;
            dbHelper.updateProductsByID(currentProduct.id, $('.right_side_wide_block .kategorii_pocetna .slika_proiz_stick a').attr('href'), $('.sku').text(), $('.product-short-description > p').text().trim(), function () {}  );

        $('.posted_in > a').each((i,el) =>{
            dbHelper.getCategoryByURL($(el).attr('href'), function (category) {
                if(category.length === 0){
                    console.log('cat not found');
                }else{
                    dbHelper.insertProductCategories(currentProduct.id, category[0].id, function () {})
                }
            });

            // Category.findOne({url: $(el).attr('href')}, function (err, category) {
            //     if(category == null){
            //         console.log('cat not found');
            //     }else{
            //         if(currentProduct.categories.indexOf(category._id) <= -1){
            //             currentProduct.categories.push(category._id);
            //
            //         }
            //         console.log('cat found');
            //         console.log(currentProduct);
            //     }
            //     currentProduct.save(function (err) {
            //         if(err) console.error(err);
            //     });
            //
            // });

        });



            console.log("finished");
    }


};


dbHelper.getProductByID(239, function (products) {
    if(products.length === 0){
        console.log('product not found');
    }else{
        currentProduct = products[0];
        console.log(currentProduct);
        startCrawler();
    }
});




