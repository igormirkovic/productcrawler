class Helper{

  static spinnerStart = null;
  static spinnerEnd = null;

  static fetchData = (url, callBack) => {
    Helper.spinnerStart();

    fetch(url)
      .then(res => res.json())
      .then(res => {
        Helper.spinnerEnd();
        callBack(res);
      })
  };


  static postData = (url, config,  callBack) => {
    Helper.spinnerStart();

    fetch(url,config)
      .then(res => res.json())
      .then(res => {
        Helper.spinnerEnd();
        callBack(res);
      })
  }
}

export default Helper;





