import React, { Component, Fragment } from 'react';
import { Button, Modal } from 'react-bootstrap';
import CheckboxTree from  'react-checkbox-tree';


class Crawl extends Component{
  constructor(props){
    super(props);
    this.state = {
      loading: 0,
      progress: null,
      show: false,
      checked: [],
      expanded: [],
      nodes: [],
      categories: []
    }
  }

  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        value: cat.name,
        original: cat,
        children: []
      };

      if(cat.parent_id == null){
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else{
        const parent = allNodes[cat.parent_id];
        if(parent){
          parent.children.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });
    this.setState({nodes});
  };

  getCategoriesCrawl = (source) =>{
    fetch('/api/getcategoriescrawlTinex')
      .then(res => res.json())
      .then(res => this.setState({loading: res.state}))
      .then(() =>{
        if(this.state.loading === 1){
          this.updateCategoriesCrawl();
        }
        }
      )
  };

  updateCategoriesCrawl = () =>{
    setTimeout(() => {
    fetch('/api/getcrawlinfo')
      .then(res => res.json())
      .then(res => this.setState({ loading: res.state }))
  },2000);
  };

  componentDidMount(){
    if(this.state.loading === 1){
      setTimeout(() => this.updateCategoriesCrawl(), 2000)
    }
    this.fetchCategoriesBySourceID(1);
  }

  makeChecked = (array) =>{
    array.forEach((el, index) =>{
      if(this.state.checked.indexOf(el) < -1)
        this.setState(prevState => ({
        checked: [...prevState.checked, el]
      }));
    });
    console.log(this.state.checked);
  };

  getCrawlableCategories = (sourceid) =>{
     fetch('/api/crawlablecategories/'+ sourceid)
      .then(res => res.json())
      .then(res => this.makeChecked(res))
  };


  fetchCategoriesBySourceID = (sourceid) => {
    fetch('/api/getCategoriesBySourceID/' + sourceid)
      .then(res => res.json())
      .then(res => {
        this.setState({ categories: res });
        this.makeTree();
        this.getCrawlableCategories(sourceid);
      })
  };


  sendToCrawl = (sourceid) =>{
    let data = [];
    this.state.checked.forEach((el, index) =>{
      data.push(el)
    });
    fetch('/api/sourcecategories/' + sourceid, {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(() => this.setState({show:false}))
  };

  fetchProductsListing = () =>{
    console.log('begin crawl');
    fetch('/api/getproductscrawlTinex')
      .then(res => res.json())
      .then(() => console.log('success'))
  };


  render(){

    if(this.state.show){
      var showmodal = <Modal
        show={this.state.show}
        onHide={() => this.setState({show: false})}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
              Categories
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='modal-body-config'>
          <CheckboxTree
            nodes={this.state.nodes}
            checked={this.state.checked}
            expanded={this.state.expanded}
            onCheck={checked => this.setState({ checked })}
            onExpand={expanded => this.setState({ expanded })}
          />
          <Button onClick={() => this.sendToCrawl(1)} className="crawl-categories">Crawl categories</Button>
        </Modal.Body>
      </Modal>
    }

    return(
      <Fragment>
      <div className={this.state.loading === 1 ? "loader": "none"}/>
      <div className="crawl-container">
        <div className="crawl-text">Tinex</div>
        <Button onClick={() => this.getCategoriesCrawl()} className="crawl-button">Get categories</Button>
        <span className="categories-count">You selected {this.state.checked.length} categories</span>
        <Button onClick={ () => this.fetchCategoriesBySourceID(1)} disabled={this.state.loading === 1} className="crawl-button">Config</Button>
        <Button disabled={this.state.loading === 1} onClick={this.fetchProductsListing} className="crawl-button">Test</Button>
        <div className="crawl-text">Paket</div>
        <Button disabled={this.state.loading === 1} className="crawl-button">Get categories</Button>
        <Button disabled={this.state.loading === 1} className="crawl-button">Config</Button>
        <Button disabled={this.state.loading === 1} className="crawl-button">Crawl products</Button>
        <div className="crawl-text">Reptil</div>
        <Button disabled={this.state.loading === 1} className="crawl-button">Get categories</Button>
        <Button disabled={this.state.loading === 1} className="crawl-button">Crawl categories</Button>
        <Button disabled={this.state.loading === 1} className="crawl-button">Crawl products</Button>
      </div>
        <div>{showmodal}</div>
      </Fragment>
    );
  }

}

export default Crawl;

