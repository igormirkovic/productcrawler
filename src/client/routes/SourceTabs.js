import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import TreeMenu from 'react-simple-tree-menu';
import Button from 'react-bootstrap/Button';




class SourceTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nodes: [],
      categories: [],
      nodesIndexes: {},
      categoryProducts: null,
      showSearchBox: false,
      products: [],
      nodesProducts: [],
      selectedProduct: null
  }
  }
  makeTree = () => {
    const nodesIndexes = {};
    const nodes = [];
    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        original: cat,
        nodes: [],
        height: 0,
        mylevel: 0,
      };


      if(cat.parent_id == null){
        nodes.push(curNode);
        nodesIndexes[cat.id] = curNode;
      }
      else{
        const parent = nodesIndexes[cat.parent_id];
        if(parent){
          parent.nodes.push(curNode);
          nodesIndexes[cat.id] = curNode;
        }
      }
    });
    this.setState({nodes, nodesIndexes});

  };
  makeTreeProducts = () =>{
      const nodesProducts = [];
      this.state.products.forEach((prod, index) =>{
        const curNode = {
          index: prod.id,
          key: prod.id,
          label: prod.name,
          original: prod,
          nodes:[]
        };
        nodesProducts.push(curNode);
      });
      this.setState({ nodesProducts })
  };

  fetchCategories = () => {
    window.globalHelper.fetchData('/api/getCategoriesBySourceID/' + this.props.source_id, (res) => {
      this.setState({ categories: res });
      this.makeTree();
    })
  };


  componentDidMount(){
    this.fetchCategories();


  }


  getAllCategoryIDsFromParentCat = (parent) => {
     let ids = [parent.index];
     parent.nodes.forEach(node=>{
       ids = ids.concat(this.getAllCategoryIDsFromParentCat(node))
     });
     return ids;
   };

   getProducts = (node) =>{
     console.log(node);
     this.setState({categoryProducts: node.index});
     const ids = this.getAllCategoryIDsFromParentCat(this.state.nodesIndexes[node.index]);
     window.globalHelper.postData('/api/getProductsByIDS/', {
         method: 'POST',
        body: JSON.stringify({ids}),
         headers: {
           'Content-Type': 'application/json'
         }
       },(res) =>{
         this.setState({products: res, edit: true});
         this.makeTreeProducts();
     }
     )};



  fetchProductsSmall = () => {
    window.globalHelper.fetchData('/api/sourceproductsProductID/' + this.props.productDetail.id, (res) => {
      this.props.handleSourceProducts(res);
    })
  };


  updateSourceProductID = () => {
    let data = {
      insertID: this.props.productDetail.id,
      updateID: this.state.selectedProduct.original.id
    };
    console.log(data)
    window.globalHelper.postData('/api/sourceproductsupdate', {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }, () =>{
      this.props.onFetchProducts(this.props.currentCategory);
      if(!this.props.addingMode){
        this.fetchProductsSmall();
        this.props.handleModal();
      }else{
        this.props.onHandleModal()
    }})
  };


  render(){
    return(
      <Fragment>
        <div className='add-sources'>
      <div className='tab-table'>
        <TreeMenu data={this.state.nodes} onClickItem={(e) => this.getProducts(e)}/>
      </div>
        <div className={this.state.categoryProducts ? 'products-tab' : 'hide'}>
          <TreeMenu data={this.state.nodesProducts} onClickItem={(e) => this.setState({selectedProduct: e})}/>
        </div>
        <Button onClick={() => this.updateSourceProductID()} className={this.state.selectedProduct ? 'view-button add-sources-button' : 'hide'}>Add product</Button>
        </div>
      </Fragment>
    );
  }
  }



export default SourceTabs;
