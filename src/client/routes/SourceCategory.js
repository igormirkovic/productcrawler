import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import TreeView from 'react-simple-tree-menu';
import ReactTable from 'react-table';
import { Button, Modal } from 'react-bootstrap';
import matchSorter from 'match-sorter';
import SourceProductDetail from './SourceProductDetail';
import HeartCheckbox from 'react-heart-checkbox';
import 'pretty-checkbox/dist/pretty-checkbox.min.css'


class SourceCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      categories: [],
      edit: false,
      currentProducts: [],
      nodes: [],
      nodesIndexes: {},
      showModal: false,
      productDetail: null,
      currentProduct: null,
      crawlableCategories: [],
      logs: [],
      loaded: false,
      filtered: [],
      filterAll: '',
      currentNode: null
    };
  }


  componentDidMount() {
    window.globalHelper.fetchData(`/api/getCategoriesBySourceID/${this.props.sourceid}`, (res) => {
      this.setState({ categories: res });
      this.makeTree();
    });
  }

    filterAll = (e) => {
      const { value } = e.target;
      const filterAll = value;
      const filtered = [{ id: 'all', value: filterAll }];
      this.setState({ filterAll, filtered });
    }

    onFilterChange = (filtered) => {
      if (filtered.length > 1 && this.state.filterAll.length) {
        // NOTE: this removes any FILTER ALL filter
        const filterAll = '';
        this.setState({ filtered: filtered.filter(item => item.id != 'all'), filterAll });
      } else this.setState({ filtered });
    }

    makeTree = () => {
      const nodesIndexes = {};
      const nodes = [];
      this.state.categories.forEach((cat, index) => {
        const curNode = {
          index: cat.id,
          key: cat.id,
          label: cat.name,
          original: cat,
          nodes: [],
          height: 0,
          mylevel: 0,
        };


        if (cat.parent_id == null) {
          nodes.push(curNode);
          nodesIndexes[cat.id] = curNode;
        } else {
          const parent = nodesIndexes[cat.parent_id];
          if (parent) {
            parent.nodes.push(curNode);
            nodesIndexes[cat.id] = curNode;
          }
        }
      });

      this.setState({ nodes, nodesIndexes });
    };

    getAllCategoryIDsFromParentCat = (parent) => {
      let ids = [parent.index];
      parent.nodes.forEach((node) => {
        ids = ids.concat(this.getAllCategoryIDsFromParentCat(node));
      });
      return ids;
    }

    getProducts = (node) => {
      if (node.index == -1) {
        window.globalHelper.fetchData(`/api/favorites/${this.props.sourceid}`, (res) => {
          this.setState({ products: res, edit: true, currentNode: node });
        });
      } else {
        const ids = this.getAllCategoryIDsFromParentCat(this.state.nodesIndexes[node.index]);
        window.globalHelper.postData('/api/getProductsByIDS/', {
          method: 'POST',
          body: JSON.stringify({ ids }),
          headers: {
            'Content-Type': 'application/json'
          }
        }, res => this.setState({ products: res, edit: true, currentNode: node }));
      }
    };


    fetchSourceProductDetail = (row) => {
      window.globalHelper.fetchData(`/api/sourceproducts/${row.original.id}`, (res) => {
        this.setState({ productDetail: res[0] },);
        this.getProductLogs(row.original.id);
        this.setState({ showModal: true, loaded: true });
      });
    };




    getProductLogs = (id) => {
      const data = [];
      fetch(`/api/sourceproductslogs/${id}`)
        .then(res => res.json())
        .then((res) => {
          res.forEach(((element) => {
            const a = element.crawl_date.split('T');
            const date = a[0];
            const dateArr = date.split('-');
            const year = dateArr[0];
            const mounth = dateArr[1];
            const day = dateArr[2];
            const time = a[1];
            const timeArr = time.split(':');
            const hour = timeArr[0];
            const minute = timeArr[1];
            const seconds = timeArr[2].slice(0, 2);
            element.crawl_date = `${day}/${mounth}/${year} ${hour}:${minute}:${seconds}`;
            data.push(element);
          }));
        })
        .then(() => this.setState({ logs: data }));
    };

    checkPrice = (row) => {
      if (row && row.original && row.original.price) {
        return `${row.original.price},00 ден`;
      }
      return '/';
    };

    handleCheck =(row) =>{
      let id = row.original.id;
      if(row.original.favorites == 0 || row.original.favorites == null){
        window.globalHelper.postData('/api/favorites/', {
          method: 'POST',
          body: JSON.stringify({id}),
          headers: {
            'Content-Type': 'application/json'
          }
        }, () => this.getProducts(this.state.currentNode))
      }else{
        window.globalHelper.postData('/api/favoritesOf/', {
          method: 'POST',
          body: JSON.stringify({id}),
          headers: {
            'Content-Type': 'application/json'
          }
        }, () => this.getProducts(this.state.currentNode))
      }


    }


    render() {
      let sourceproductdetails = null;
      if (this.state.productDetail != null && this.state.loaded == true) {
        sourceproductdetails = (
          <SourceProductDetail
            key={`SourceProductDetail_${this.state.productDetail}`}
            logs={this.state.logs}
            productDetail={this.state.productDetail}
          />
        );
      }

      return (
        <Fragment>
          <Modal
            size="lg"
            show={this.state.showModal}
            onHide={() => this.setState({ showModal: false, productDetail: null, logs: [] })}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                  {this.state.productDetail != null ? this.state.productDetail.name : ''}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body className="modal-body-config">
              {sourceproductdetails}
            </Modal.Body>
          </Modal>
          <div className="container-table source-categories">
            <TreeView
              data={this.state.nodes}
              onClickItem={
                            e => this.getProducts(e)
                        }
            />
          </div>
          <div className={this.state.edit ? 'showMe' : 'hide'}>
            <div className="table-content">
              <div className="filter-found">
                <div className="search-icon" />
                <input placeholder="Type and search" value={this.state.filterAll} onChange={this.filterAll} />
              </div>
              <ReactTable
                className="source-products"
                getTdProps={(state, rowInfo, column, instance) => ({
                  onClick: (e, handleOriginal) => {
                    this.setState({ currentProduct: rowInfo.original });
                    if (handleOriginal) {
                      handleOriginal();
                    }
                  }
                })
                                }
                resizable={false}
                data={this.state.products}
                ref={(r) => {
                  this.selectTable = r;
                }}
                filtered={this.state.filtered}
                defaultFilterMethod={(filter, row) => String(row[filter.id].name) === filter.value}
                onFilteredChange={e => this.onFilterChange(e)}
                columns={[
                  {
                    Header: 'Name',
                    accessor: 'name',
                    width: 350,
                    filterable: true
                  },
                  {
                    Header: 'Price',
                    accessor: 'price',
                    Cell: row => this.checkPrice(row)
                  },
                  {
                    Header: 'View',
                    width: 200,
                    Cell: row => (
                        <div>
                      <Button
                        size="sm"
                        onClick={() => this.fetchSourceProductDetail(row)}
                        className="view-button"
                      >View</Button>
                        </div>
                    )
                  },
                  {
                    // NOTE - this is a "filter all" DUMMY column
                    // you can't HIDE it because then it wont FILTER
                    // but it has a size of ZERO with no RESIZE and the
                    // FILTER component is NULL (it adds a little to the front)
                    // You culd possibly move it to the end
                    Header: 'All',
                    id: 'all',
                    show: false,
                    width: 0,
                    resizable: false,
                    sortable: false,
                    Filter: () => {
                    },
                    getProps: () => ({
                      // style: { padding: "0px"}
                    }),
                    filterMethod: (filter, rows) => {
                      // using match-sorter
                      // it will take the content entered into the "filter"
                      // and search for it in EITHER the firstName or lastName
                      const result = matchSorter(rows, filter.value, {
                        keys: [
                          'name',
                        ],
                        threshold: matchSorter.rankings.CONTAINS
                      });
                      return result;
                    },
                    filterAll: true,
                  },
                ]}
                defaultPageSize={15}
              />
            </div>
          </div>
        </Fragment>
      );
    }
}


export default SourceCategory;
