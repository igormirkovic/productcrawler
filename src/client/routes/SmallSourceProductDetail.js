import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import { Button, Image } from 'react-bootstrap';
import Modal from 'react-bootstrap/es/Modal';
import SourceProductDetail from './SourceProductDetail';


class SmallSourceProductDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentProduct: null,
      showModal: false,
      onRemove: this.props.onRemove,
      logs: []
    }
  }

  fetchProductDetail = () =>{
    fetch('/api/sourceproducts/'+ this.props.productDetail.id)
      .then(res => res.json())
      .then(res => this.setState({currentProduct: res[0]}))
      .then(() => this.getProductLogs(this.props.productDetail.id))
      .then(this.setState({showModal: true}, console.log('done')))
  };

  deleteProductSource = () =>{
    let names = [];
    names.push(this.props.productDetail.name);

    let data = {
      insertID: null,
      names: names
    };
    fetch('/api/sourceproducts', {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(response => {
        console.log('Success:', JSON.stringify(response));
      })
      .then(() =>{
        console.log('removed')
      } )
      .then(this.state.onRemove(this.props.productDetail))
  };
  getProductLogs = (id) =>{
    let data = [];
    fetch('/api/sourceproductslogs/' + id)
      .then(res => res.json())
      .then((res) => res.forEach((function(element) {
        let a = element.crawl_date.split("T");
        let date = a[0];
        let dateArr = date.split("-");
        let year = dateArr[0];
        let mounth = dateArr[1];
        let day = dateArr[2];
        let time = a[1];
        let timeArr = time.split(":");
        let hour = timeArr[0];
        let minute = timeArr[1];
        let seconds = timeArr[2].slice(0,2);
        element.crawl_date = day + '/' + mounth + '/' + year + " " +hour+':'+minute+':'+seconds;
        console.log(element);
        data.push(element);
      })))
      .then(() => this.setState({logs: data}))
  };

    checkProductMarket = (sourceid) =>{
        switch (sourceid) {
            case 1:
                return 'Tinex'
                break;
            case 2:
                return 'Paket'
                break;
            case 3:
                return 'Reptil'
                break;
        }
    }





  render() {
    let productdetail = null;
    if(this.state.currentProduct != null){
      productdetail = <SourceProductDetail productDetail={this.state.currentProduct} logs={this.state.logs}/>
    }


    return (
      <Fragment>
        <Modal
          size="lg"
          show={this.state.showModal}
          onHide={() => this.setState({showModal: false})}
          aria-labelledby="example-modal-sizes-title-lg"
          className="modal-width"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-lg">
              Source Product Detail
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={'modal-body-config'}>
            {productdetail}
          </Modal.Body>
        </Modal>
        <div className='block-source-product'>
            <div className='product-source-market'>{this.checkProductMarket(this.props.productDetail.sourceid)}</div>
        <div className="image-div-small">
          <Image className='image-url' src={this.props.productDetail.image_url} rounded/>
          <div className="buttons-div">
            <Button className="source-product-detail" onClick={() => this.fetchProductDetail()}>View</Button>
            <Button className="source-product-detail" onClick={() => this.deleteProductSource()}>Remove</Button>
          </div>
        </div>
          <div className="source-category-block">
              <div className='source-category-market'>{this.checkProductMarket(this.props.productDetail.source_id)}</div>
            <div className="source-category-name">{this.props.productDetail.name}</div>
            <div className="source-category-price">{this.props.productDetail.price +",00 ДЕН"}</div>
            <div className="source-category-description">{this.props.productDetail.description}</div>
          </div>
        </div>
      </Fragment>
    );
  }
}


export default SmallSourceProductDetail;
