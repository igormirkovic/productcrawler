import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import CategoryPart from './CategoryPart';


class OneProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProducts: []
    }

  }

  handleProducts = (product) =>{
    var found = this.state.selectedProducts.find(function(element) {
      return element.source_id === product.source_id;
    });
    if(found) {
      this.setState({
        selectedProducts: this.state.selectedProducts.filter((_, i) => i !== this.state.selectedProducts.indexOf(found))
      });
    }

    this.setState(prevState => ({
      selectedProducts: [...prevState.selectedProducts, product]
    }), () => console.log(this.state.selectedProducts))  };


  render(){
    return(
      <Fragment>
        <CategoryPart title="us" onSelectProducts={this.handleProducts} styleOneProduct={true} OneProductOur={true}/>
        <CategoryPart onSelectProducts={this.handleProducts} styleOneProduct={true} dropDownShow={true}/>
      </Fragment>

  );
  }
}


export default OneProduct;
