import React, { Component, Fragment } from 'react';
import {  Button } from 'react-bootstrap';
import TreeMenu from 'react-simple-tree-menu';
import MyVerticallyCenteredModal from './MyVerticallyCenteredModal'





class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalShow: false,
      categories: [],
      mode: 0,
      currentCategory: null,
      nodes: []
    };
  }


  modalClose = () => {
    this.setState({ mode: 0 });
  };

  fetchCategories = () => {
    fetch('/api/categories')
      .then(res => res.json())
      .then(res => this.setState({ categories: res, mode: 0, currentCategory: null }))
      .then(() => this.makeTree());
  };

  componentDidMount() {
    this.fetchCategories();
  }

  saveCategory = (name) => {
    let curCategory = null;
    let method = '';
    switch (this.state.mode) {
      case 1:
        curCategory = {
          name: name,
          parent_id: null
        };
        method= 'POST';
        break;
      case 2:
        console.log(this.state.currentCategory);
        curCategory = {
          name: name,
          parent_id: this.state.currentCategory.index
        };
        method = 'POST';
        break;
      case 3:
        curCategory = this.state.currentCategory.original;
        curCategory.name = name;
        method = 'PUT';
        break;
    }

    fetch('/api/categories', {
      method: method,
      body: JSON.stringify(curCategory),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(response => {
        this.setState({
          mode: 0,
          currentCategory: null
        });
        this.fetchCategories();
      })
      .catch(error => console.error('Error:', error));

  };


  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        original: cat,
        nodes: []
      };

      if(cat.parent_id == null){
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else{
        const parent = allNodes[cat.parent_id];
        if(parent){
          parent.nodes.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });

    this.setState({nodes});
  };

  render() {
    let modal = null;
    if(this.state.mode > 0){
      modal = <MyVerticallyCenteredModal
        name={this.state.mode === 3 ? this.state.currentCategory.label : ''}
        show={true}
        onHide={this.modalClose}
        onSave={this.saveCategory}
        mode={this.state.mode}
      />
    }

    return (
      <Fragment>
        <div className="container-table">
          <div className='categories'>
            <div className='buttons-div'>
              <Button onClick={ ()=> this.setState({ modalShow: true, mode: 1 })}>Add</Button>
              <Button disabled={this.state.currentCategory == null} onClick={()=> this.setState({ modalShow: true, mode: 2 })}>Add in category</Button>
              <Button disabled={this.state.currentCategory == null} onClick={()=> this.setState({ modalShow: true, mode: 3 })}>Edit category</Button>
            </div>
          <TreeMenu data={this.state.nodes} onClickItem={(currentCategory) => this.setState({currentCategory})} />
        </div>
        </div>
        {modal}
      </Fragment>
    );
  }
}


export default Categories;
