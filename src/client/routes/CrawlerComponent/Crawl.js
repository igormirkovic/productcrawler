import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import PopUpCrawler from './PopUpCrawler';
import ProgressBar from 'react-bootstrap/ProgressBar'


class Crawl extends Component{
  constructor(props){
    super(props);
    this.state = {
      loading: 0,
      show: false,
      categories: [],
      productsListing: 0,
      sourceid: 0,
      tinexNumber: 0,
      paketNumber: 0,
      reptilNumber: 0,
      tinexNumberCrawlable: 0,
      paketNumberCrawlable: 0,
      reptilNumberCrawlable: 0,
      nodes: [],
      status: {
        data: {
          type: null,
          products: 0,
          categories: 0,
          range: 0
        },
        state: 0
      }
    }
  }

  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        value: cat.name,
        original: cat,
        children: []
      };
      if (cat.parent_id == null) {
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else {
        const parent = allNodes[cat.parent_id];
        if (parent) {
          parent.children.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });
    this.setState({ nodes });
  };


  getCategoriesCrawl= (sourceid) =>{
    switch (sourceid) {
      case 1:
        console.log('Crawling Tinex');
        window.globalHelper.fetchData('/api/getcategoriescrawlTinex', (res) =>{
          this.setState({status: res});
        })
        break;
      case 2:
        console.log('Crawling Paket');
        window.globalHelper.fetchData('/api/getcategoriescrawlPaket', (res) =>{
          this.setState({status: res});
        })
        break;
      case 3:
        console.log('Crawling Reptil')
        window.globalHelper.fetchData('/api/getcategoriescrawlReptil', (res) =>{
          this.setState({status: res});
        })
        break;

    }
    window.globalHelper.fetchData('/api/getcategoriescrawlTinex', (res) =>{
      this.setState({status: res});
    })
  };

  getCategoriesCrawlPaket = () =>{
    fetch('/api/getcategoriescrawlPaket')
      .then(res => res.json())
      .then(res => this.setState({loading: res.state}))
      .then(() =>{
          if(this.state.loading === 1){
            this.updateCategoriesCrawl();
          }
        }
      )
  };

  getCategoriesCrawlReptil = () =>{
    fetch('/api/getcategoriescrawlReptil')
      .then(res => res.json())
      .then(res => this.setState({status: res}))
      .then(() =>{
          if(this.state.loading === 1){
            this.updateCategoriesCrawl();
          }
        }
      )
  };

  numberOfCategories = () =>{
    window.globalHelper.fetchData('/api/getcountcrawlable/', (res) =>{
      this.setState({
        tinexNumberCrawlable: res[0].crawlableCount,
        paketNumberCrawlable: res[1].crawlableCount,
        reptilNumberCrawlable: res[2].crawlableCount,
        show: false
      })
    });
  };


  getCrawlState = () =>{
    if(this.state.status.state === 1) {
      fetch('/api/getcrawlinfo')
        .then(res => res.json())
        .then(res => this.setState({status: res}))
    }
  };
  updateCategoriesCrawl = () =>{
    setTimeout(() => {
    fetch('/api/getcrawlinfo')
      .then(res => res.json())
      .then(res => this.setState({ loading: res.state }))
  },2000);
  };
    componentDidMount(){
      this.updateCategoriesCrawl();
     this.interval = setInterval(() => {
        this.getCrawlState();
      }, 1000);


  }
  componentWillUnmount(){
    clearInterval(this.interval);
  }
  fetchCategoriesBySourceID = (sourceid) => {
    window.globalHelper.fetchData('/api/getCategoriesBySourceID/' + sourceid, (res) =>{
      this.setState({ categories: res });
      this.makeTree();
      this.setState({sourceid: sourceid, show: true});
    })
  };
    handlePopUp = () =>{
      this.numberOfCategories();
    };

  fetchProductsListingTinex = () =>{
    this.setState({productsListing: 1});
    fetch('/api/getproductscrawlTinex')
      .then(res => res.json())
      .then(res => this.setState({status: res}))
      .then(() => console.log('success'))
  };

  fetchProductsListingReptil = () =>{
    fetch('/api/getproductscrawlReptil')
      .then(res => res.json())
      .then(() => console.log('success'))
  };

  fetchProductsListingPaket = () =>{
    this.setState({productsListing: 1});
    fetch('/api/getproductscrawlPaket')
      .then(res => res.json())
      .then(() => console.log('success'))
  };

  handleOnClick = (sourceid) =>{
    this.fetchCategoriesBySourceID(sourceid);
  };

  render(){
    if(this.state.show){
      var showmodal = <PopUpCrawler onUpdateCategories={this.fet}  key={"_" + this.state.sourceid} sourceid={this.state.sourceid} show={this.state.show} nodes = {this.state.nodes} numberOfCategories={this.numberOfCategories}/>
    }
    let progressbar = null;
    if(this.state.status.state === 1 && this.state.productsListing === 1){
      progressbar = <div className='progress-bar-class'>
        <ProgressBar animated now={(this.state.status.data.categories - this.state.status.data.range) / this.state.status.data.categories * 100} />
        <div className='crawler-status-products'>
          <span className='new-line'>Crawled {this.state.status.data.products} products</span>
          <span className='new-line'>In {this.state.status.data.categories} categories</span>
          <span className='new-line'>Remaining categories {this.state.status.data.range}</span>
        </div>
      </div>
    }
    return(
      <Fragment>
      <div className="crawl-container">
        <div className="crawl-text">Tinex</div>
        <Button onClick={() => {
          this.getCategoriesCrawlTinex();
          this.getCrawlState();
        }} disabled={this.state.status.state === 1} className="crawl-button view-button">Get categories</Button>
        <span className="categories-count">You selected {this.state.tinexNumberCrawlable} categories</span>
        <Button onClick={() => this.handleOnClick(1)} disabled={this.state.status.state === 1} className="crawl-button view-button">Config</Button>
        <Button onClick={() =>{
          this.fetchProductsListingTinex();
          this.getCrawlState();
        } } disabled={this.state.status.state === 1} className="crawl-button view-button">Crawl</Button>
        <div className="crawl-text">Paket</div>
        <Button disabled={this.state.status.state === 1} onClick={() => this.getCategoriesCrawlPaket()} className="crawl-button view-button">Get categories</Button>
        <span className="categories-count">You selected {this.state.paketNumberCrawlable} categories</span>
        <Button onClick={() => this.handleOnClick(2)} disabled={this.state.status.state === 1} className="crawl-button view-button">Config</Button>
        <Button onClick={() => {
          this.fetchProductsListingPaket();
          this.getCrawlState();
        }} disabled={this.state.status.state === 1} className="crawl-button view-button">Crawl</Button>
        <div className="crawl-text">Reptil</div>
        <Button disabled={this.state.status.state === 1} onClick={() => this.getCategoriesCrawlReptil()} className="crawl-button view-button">Get categories</Button>
        <span className="categories-count">You selected {this.state.reptilNumberCrawlable} categories</span>
        <Button onClick={() => this.handleOnClick(3)} disabled={this.state.status.state === 1} className="crawl-button view-button">Config</Button>
        <Button disabled={this.state.status.state === 1} onClick={() => this.fetchProductsListingReptil()} className="crawl-button view-button">Crawl</Button>
      </div>
        {progressbar}
        <div>{showmodal}</div>


      </Fragment>
    );
  }

}

export default Crawl;

