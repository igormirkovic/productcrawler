import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import PopUpCrawler from './PopUpCrawler';
import ProgressBar from 'react-bootstrap/ProgressBar'
import CrawlerSource from './CrawlerSource';

let data = {};

class CrawlerFinal extends Component {
  intervalID = 0;
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      sourceid: 0,
      nodes: [],
      loaded: false,
      productListing: 0,
      tinexNumberCrawlable: 0,
      paketNumberCrawlable: 0,
      reptilNumberCrawlable: 0,
      logsTinex:[],
      logsPaket:[],
      logsReptil: [],

      status: {
        data: {
          sourceid: 0,
          products: 0,
          categories: 0,
          range: 0,
          error: ''
        },
        state: 0
      }
    }
  }

  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        value: cat.name,
        original: cat,
        children: []
      };
      if (cat.parent_id == null) {
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else {
        const parent = allNodes[cat.parent_id];
        if (parent) {
          parent.children.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });
    this.setState({ nodes });
  };


  getCategoriesCrawl= (sourceid) => {
    switch (sourceid) {
      case 1:
        console.log('Crawling Tinex');
        window.globalHelper.fetchData('/api/getcategoriescrawlTinex', (res) => {
          this.setState({ status: res });
        })
        break;
      case 2:
        console.log('Crawling Paket');
        window.globalHelper.fetchData('/api/getcategoriescrawlPaket', (res) => {
          this.setState({ status: res });
        })
        break;
      case 3:
        console.log('Crawling Reptil')
        window.globalHelper.fetchData('/api/getcategoriescrawlReptil', (res) => {
          this.setState({ status: res });
        })
        break;

    }
  }




  numberOfCategories = () =>{
    window.globalHelper.fetchData('/api/getcountcrawlable/', (res) =>{
      console.log(res);
      this.setState({
        tinexNumberCrawlable: res[0].crawlableCount,
        paketNumberCrawlable: res[1].crawlableCount,
        reptilNumberCrawlable: res[2].crawlableCount,
        show: false,
        loaded: true
      })
    });
  };


  getCrawlState = () =>{
    if(this.state.status.state === 0 && this.state.status.data.sourceid !== 0){
      console.log(this.intervalID);
      this.postCrawlingLog(this.state.status.data.sourceid);
      clearInterval(this.intervalID)
    }
    if(this.state.status.state === 1) {
      fetch('/api/getcrawlinfo')
        .then(res => res.json())
        .then(res => this.setState({status: res}))
    }
  };
  updateCategoriesCrawl = () =>{
    setTimeout(() => {
      fetch('/api/getcrawlinfo')
        .then(res => res.json())
        .then(res => this.setState({ loading: res.state }))
    },2000);
  };
  componentDidMount(){
    console.log('changing state');
    this.numberOfCategories();
    this.updateCategoriesCrawl();
    this.fetchCrawlingLog(1);
    this.fetchCrawlingLog(2);
    this.fetchCrawlingLog(3);
  }

  getCrawlStateInterval = () =>{
    this.intervalID = setInterval(() => {
      this.getCrawlState();
    }, 1000);
    if(this.state.status.state === 0 && this.state.status.data.sourceid !== 0){
      clearInterval(this.intervalID);
    }
  }

  fetchCategoriesBySourceID = (sourceid) => {
    window.globalHelper.fetchData('/api/getCategoriesBySourceID/' + sourceid, (res) =>{
      this.setState({ categories: res });
      this.makeTree();
      this.setState({sourceid: sourceid, show: true});
    })
  };
  handlePopUp = () =>{
    this.numberOfCategories();
  };

  postCrawlingLog = (sourceid) =>{
    let data = this.state.status;
    window.globalHelper.postData('/api/crawlinglogs/',{
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }},(res) =>{
      this.fetchCrawlingLog(sourceid)
      clearInterval(this.intervalID)
      }
  )
  }

  formatingDate = (log) =>{
    console.log(log);
    let a = log.date.split("T");
    let date = a[0];
    let dateArr = date.split("-");
    let year = dateArr[0];
    let mounth = dateArr[1];
    let day = dateArr[2];
    let time = a[1];
    let timeArr = time.split(":");
    let hour = timeArr[0];
    let minute = timeArr[1];
    let seconds = timeArr[2].slice(0,2);
    log.datelog = day + '/' + mounth + '/' + year + " " +hour+':'+minute+':'+seconds;
  }

  fetchCrawlingLog = (sourceid) =>{
    window.globalHelper.fetchData('/api/crawlinglogs/' + sourceid, (res) =>{
      switch (sourceid) {
        case 1:
          res.forEach((log) =>{
            this.formatingDate(log);
          })
          this.setState({logsTinex: res})
          break;
        case 2:
          res.forEach((log) =>{
            this.formatingDate(log);
          })
          this.setState({logsPaket: res})
          break;
        case 3:
          res.forEach((log) =>{
            this.formatingDate(log);
          })
          this.setState({logsReptil: res})
          break;
      }
      console.log(res);
    })
  }

  fetchProductListing = (sourceid) =>{
    console.log('fetch');
    this.setState({productListing: sourceid});
    switch (sourceid) {
      case 1:
        fetch('/api/getproductscrawlTinex')
          .then(res => res.json())
          .then(res =>{
            this.setState({ status: res }, () => console.log(res))
            this.getCrawlStateInterval()
          } )
        break;
      case 2:
        fetch('/api/getproductscrawlPaket')
          .then(res => res.json())
          .then(res =>{
            this.setState({ status: res }, () => console.log(res))
            this.getCrawlStateInterval()
          } )

        break;
      case 3:
        fetch('/api/getproductscrawlReptil')
          .then(res => res.json())
          .then(res =>{
            this.setState({ status: res }, () => console.log(res))
            this.getCrawlStateInterval()
          } )
          break;
    }
  }

  startAll = () =>{
    window.globalHelper.fetchData('/api/startall', () =>{
      console.log('started')
    })
  }

  handleOnClick = (sourceid) =>{
    this.fetchCategoriesBySourceID(sourceid);
  };

  render() {

    let ModalConfig = null;
    if(this.state.show){
      ModalConfig = <PopUpCrawler onUpdateCategories={this.getCategoriesCrawl}  key={"_" + this.state.sourceid} sourceid={this.state.sourceid} show={this.state.show} nodes = {this.state.nodes} numberOfCategories={this.numberOfCategories}/>
    }
    if(this.state.loaded == true){
      return(
        <div className='crawler-container'>
          <div className='crawl-content'>
          <Button
              onClick={() => this.startAll()}
              disabled={this.state.status.state == 1} className='view-button-crawler start-all'>Start all</Button>
          {ModalConfig}
          <CrawlerSource logs={this.state.logsTinex} productListing={this.state.productListing} status={this.state.status}  onFetchProductListing={this.fetchProductListing} CrawlableCategories={this.state.tinexNumberCrawlable} onConfigClick={this.handleOnClick} sourceid={1} disabledButtons={this.state.status.state === 1} Type={'Tinex'}/>
          <CrawlerSource logs={this.state.logsPaket}  productListing={this.state.productListing} status={this.state.status} onFetchProductListing={this.fetchProductListing} CrawlableCategories={this.state.paketNumberCrawlable} onConfigClick={this.handleOnClick} sourceid={2} Type={'Paket'}/>
          <CrawlerSource logs={this.state.logsReptil} productListing={this.state.productListing} status={this.state.status} onFetchProductListing={this.fetchProductListing} CrawlableCategories={this.state.reptilNumberCrawlable} onConfigClick={this.handleOnClick} sourceid={3} Type={'Reptil'}/>
        </div>
        </div>

      )
    }else{
            return(
              <div>Loading...</div>)
    }
  }


}




  export default CrawlerFinal;

