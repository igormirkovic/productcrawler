
import React, {Component} from 'react';
import { Button, Modal } from 'react-bootstrap';
import CheckboxTree from 'react-checkbox-tree';

class PopUpCrawler extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allNodes : [],
      checked: [],
      expanded: [],
    }
  }

  getAllCategories = () =>{
    window.globalHelper.fetchData('/api/getCategoriesBySourceID/' + this.props.sourceid, (res) =>{
      this.setState({allNodes: res});
    })
  };


  makeData = () =>{
    let data = [];
    this.state.allNodes.forEach((node, index) => {
      let newNode = {
        ...node,
        crawlableNow: 0
      }
      data.push(newNode);
      this.state.checked.forEach((check, index) =>{
        if(data[data.length - 1].name === check){
          data[data.length - 1].crawlableNow = 1;
        }
      })
    })
    return data;
  }


  removeCrawlable = () =>{
    const checkedCategories = this.makeData();
    window.globalHelper.postData('/api/sourcecategories/' + this.props.sourceid, {
        method: 'PUT',
        body: JSON.stringify(checkedCategories),
        headers: {
          'Content-Type': 'application/json'
        }
      }, () => {
        this.props.numberOfCategories();
      }
    )
    }


  componentDidMount() {
    this.getAllCategories();
    window.globalHelper.fetchData('/api/crawlablecategories/' + this.props.sourceid,(res) => {
     res.forEach((el, index) => {
        if (this.state.checked.indexOf(el.name) === -1) {
          this.setState(prevState => ({
            checked: [...prevState.checked, el.name]
          }))
        }
      })
    });
  };

  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.numberOfCategories}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-custom-modal-styling-title">
            Categories
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className='modal-body-config'>
          <CheckboxTree
            nodes={this.props.nodes}
            checked={this.state.checked}
            expanded={this.state.expanded}
            onCheck={checked => this.setState({ checked })}
            onExpand={expanded => this.setState({ expanded })}
          />

        </Modal.Body>
        <Modal.Footer>
        <div className='pop-up-button-toolbar'>
        <Button onClick={() => this.props.onUpdateCategories(this.props.sourceid)}
    className="crawl-categories view-button-crawler-gray view-button-crawl">Update categories</Button>
    <Button onClick={() => this.removeCrawlable()}
            className="crawl-categories view-button-crawl">Save categories</Button>
        </div>
        </Modal.Footer>
      </Modal>
    );
  }
}



export default PopUpCrawler;

