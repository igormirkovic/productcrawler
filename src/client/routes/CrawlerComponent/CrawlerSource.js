import React, { Component, Fragment } from 'react';
import { Button } from 'react-bootstrap';
import PopUpCrawler from './PopUpCrawler';
import ProgressBar from 'react-bootstrap/ProgressBar'
import ReactTable from 'react-table';
import Progress from 'reactstrap/es/Progress';


class CrawlerSource extends Component{
  constructor(props) {
    super(props);

    }



  render(){
    console.log(this.props.status)
    let Progress = null;
    if(this.props.status.state === 1){
      Progress = <div className='progress-bar-class'>
        <ProgressBar key={'Progressbar_'+ this.props.sourceid} animated now={(this.props.status.data.categories - this.props.status.data.range) / this.props.status.data.categories * 100} />
        <div className='crawler-status-products'>
          <span className='new-line'>Crawled {this.props.status.data.products} products</span>
          <br/>
          <span className='new-line'>In {this.props.status.data.categories} categories</span>
          <br/>
          <span className='new-line'>Remaining categories {this.props.status.data.range}</span>
        </div>
      </div>
    }
    return(
      <div className='source-crawl-container'>
        <span className='crawl-logs-label'>{this.props.Type + ' crawl logs:'}</span>
        <div className='table-log'>
          <ReactTable
            className='crawler-table'
            data = {this.props.logs}
            columns={[
            {
              Header: 'Date',
              accessor: 'datelog',
              width: 200
            },
            {
              Header: 'Categories',
              accessor: 'categories',
              width: 100

            },
            {
              Header: 'Products',
              accessor: 'products',
              width: 100

            },
            {
              Header: 'Error',
              accessor: 'error',
              width: 50

            }
          ]}
            showPagination={false}
          />
        </div>
        <div className='crawl-toolbar'>
          {this.props.status.state == 1 && this.props.status.data.sourceid === this.props.sourceid ?
            (Progress): <span>{'Selected ' + this.props.CrawlableCategories +' categories'}</span>
          }
          <Button disabled={this.props.status.state == 1} onClick={() => this.props.onConfigClick(this.props.sourceid)} className='view-button-crawler-gray view-button-crawler'>Config</Button>
          <Button disabled={this.props.status.state == 1} onClick={() =>{
            console.log('crawling' +this.props.sourceid);
            this.props.onFetchProductListing(this.props.sourceid)
          }} className='view-button-crawler'>Start</Button>
        </div>
      </div>
    );
  }

}

export default CrawlerSource;

