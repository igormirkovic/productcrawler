import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import ListGroup from 'react-bootstrap/ListGroup';
import equal from 'fast-deep-equal'




class ListingProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productSelected: null
    }
  }

  getProducts = (e) =>{
    fetch('/api/getProductsByCategoryID/'+ e.index)
      .then(res => res.json())
      .then(res => this.setState({products: res}, () => console.log(this.state.products)))
  };

  componentDidUpdate(prevProps) {
    if(!equal(this.props.category, prevProps.category)) // Check if it's a new user, you can also use some unique property, like the ID  (this.props.user.id !== prevProps.user.id)
    {
      if(this.props.category !== null) {
        this.getProducts(this.props.category)
      }
    }
  }
  render(){
    const productsComp = this.state.products.map((product) =>
      <ListGroup.Item action onClick={() => {
        this.state.productSelected = product.id;
        this.props.update(product.id);
      }} key={"_"+product.id}>{product.name}</ListGroup.Item>

    );
    return(
      <ListGroup>{productsComp}</ListGroup>
    );
  }
}



export default ListingProducts;
