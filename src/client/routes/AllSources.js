import React, { Component, Fragment } from 'react';
import ReactTable from 'react-table';
import CategoryPart from './CategoryPart';
import {Button, Modal, Form} from 'react-bootstrap';
import TreeMenu from 'react-simple-tree-menu';



class AllSources extends Component {
  constructor(props){
    super(props);

    this.state = {
      selectedProducts : [],
      clickedJoin: false,
      showModal: false,
      currentCategory: null,
      categories: [],
      nodes : [],
      productName: ''
    };
    this.child = React.createRef();

  }

  sendData = () => {
    let insertID = null;
    let data = {
      name: this.state.productName,
      currentCategory: this.state.currentCategory
    };
    fetch('/api/products', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(response => {
        console.log('Success:', JSON.stringify(response));
        this.refreshProducts(response.insertId, this.state.selectedProducts)
      });


  };

  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        original: cat,
        nodes: []
      };

      if (cat.parent_id == null) {
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else {
        const parent = allNodes[cat.parent_id];
        if (parent) {
          parent.nodes.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });
    this.setState({nodes});
  };

  fetchCategories = () =>{
    fetch('/api/categories')
      .then(res => res.json())
      .then(res => this.setState({categories: res}, console.log(this.state.categories)))
      .then(() => this.makeTree())
      .then(() => this.setState({showModal: true}))

  };

  refreshProducts = (insertID, selectedProducts) =>{
    let names = [];
    selectedProducts.forEach(function (product) {
      names.push(product.name)
    });
    let data = {
      insertID: insertID,
      names: names
    };
    fetch('/api/sourceproducts', {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(response => {
        console.log('Success:', JSON.stringify(response));
      })
      .then(this.setState({showModal: false}, ()=> console.log(this.state.clickedJoin)))

  };



  handleProducts = (product) =>{
    var found = this.state.selectedProducts.find(function(element) {
      return element.source_id === product.source_id;
    });
    if(found) {
      this.setState({
        selectedProducts: this.state.selectedProducts.filter((_, i) => i !== this.state.selectedProducts.indexOf(found))
      });
    }

    this.setState(prevState => ({
      selectedProducts: [...prevState.selectedProducts, product]
    }), () => console.log(this.state.selectedProducts))  };


  render() {
    console.log(this.state.currentCategory);
    return (
      <Fragment>
        <Button disabled={this.state.selectedProducts.length === 0} className="move" onClick={this.fetchCategories}>Join</Button>
        <Modal
          show={this.state.showModal}
          onHide={() => this.setState({showModal: false, productName: '', currentCategory: null})}
          dialogClassName="modal-90w"
          aria-labelledby="example-custom-modal-styling-title"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-custom-modal-styling-title">
              Custom Modal Styling
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group>
              <Form.Control defaultValue={this.state.productName} onChange={(e) => this.setState({productName: e.target.value})} type="text" placeholder="Product name" />
            </Form.Group>
            <TreeMenu data={this.state.nodes} onClickItem={(currentCategory) => this.setState({currentCategory})} />
            <Modal.Footer>
              <Button disabled={this.state.currentCategory === null || this.state.productName === ''} onClick={() => this.sendData()} variant="success">Accept</Button>
              <Button onClick={() => this.setState({showModal: false, productName: '', currentCategory: null})} variant="danger">Cancel</Button>
            </Modal.Footer>
          </Modal.Body>
        </Modal>
       <CategoryPart ref={this.child} onSelectProducts={this.handleProducts} sourceid={1} title="Tinex"/>
       <CategoryPart ref={this.child} onSelectProducts={this.handleProducts}  sourceid={2} title="Paket"/>
       <CategoryPart ref={this.child} onSelectProducts={this.handleProducts}  sourceid={3} title="Reptil"/>
      </Fragment>
    );
  }
}


export default AllSources;
