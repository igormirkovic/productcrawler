import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import { Image, Form, Button } from 'react-bootstrap';
import ReactTable from 'react-table';
import { Chart } from "react-charts";

const data = [
  [[new Date(1,2,3), 50], [new Date(1,2,500), 10], [new Date(1,2,3), 10]]
];
class SourceProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        [1,50],
        [2,10],
        [3,10]
      ],
          }
  }


  //crawl_date: "27/06/2019 17:13:52"

  makeData = () =>{
    let finaldata = [];
    let data = [];
    let obj = {};
    switch (this.props.productDetail.source_id) {
      case 1:
        obj['label'] = 'Tinex';
        break;
      case 2:
        obj['label'] = 'Paket';
        break;
      case 3:
        obj['label'] = 'Reptil'
        break;
    }
    this.props.logs.forEach((log,index) =>{
      let dataLog = [];
      let days = log.crawl_date.split(' ')[0].split('/')[0];
      let months = log.crawl_date.split(' ')[0].split('/')[1];
      let year = log.crawl_date.split(' ')[0].split('/')[2];
      let dateString = year+'-'+months+'-'+days;
      let date = new Date(dateString);
      dataLog.push(new Date(parseInt(year),parseInt(months-1),parseInt(days)));
      dataLog.push(log.price);
      data.push(dataLog);

    });
    obj['data'] = data;
    finaldata.push(obj)
    return finaldata;
  };

    renderPriceCell = (cellInfo) => {
      if (cellInfo && cellInfo.value) {
        return cellInfo.value + ',00 ден';
      }else{
        return '/'
      }
    }

    checkProductMarket = (sourceid) =>{
        switch (sourceid) {
            case 1:
                return 'Tinex'
                break;
            case 2:
                return 'Paket'
                break;
            case 3:
                return 'Reptil'
                break;
        }
        console.log(sourceid)
    }



  render() {
    console.log(this.props.productDetail)
    return (
      <Fragment>
        <div className="image-div">
          <Image className='image-url' src={this.props.productDetail.image_url} rounded/>
        </div>
        <div className="product-details-more">
            <div className='product-detail-source-market'>{this.checkProductMarket(this.props.productDetail.source_id)}</div>
          <div className="product-detail-more-name">{this.props.productDetail.name}</div>
          <div className="product-detail-more-price">{this.props.productDetail.price + ",00 ДЕН"}</div>
          {/*<div className="product-detail-more-url">URL MAYBE FIX</div>*/}
          {/*<div className="product-detail-more-sku">{this.props.productDetail.sku}</div>*/}
          {/*<div className="product-detail-more-description"> So wound stood guest weeks no terms up ought.</div>*/}
        </div>
        <div className='chart-div'>
        <Chart
          data={this.makeData()}
          axes={[
            { primary: true, type: "time", position: "bottom" },
            { type: "linear", position: "left", hardMin: 0, hardMax: this.props.productDetail.price * 2 }
          ]}
          tooltip
        />
        </div>
        <div className="crawl-logs-text">Crawling logs:</div>
        <ReactTable
          data={this.props.logs}
          columns={[
            {
              Header: 'Name',
              accessor: 'name'
            },
            {
              Header: 'Price',
              accessor: 'price',
              Cell: (row)=> this.renderPriceCell(row)
            },
            {
              Header: 'Date',
                id: 'date',
              accessor: 'crawl_date'
            }
          ]}
          defaultSorting={[
              {
                  id: 'date',
                  desc: true
              }
          ]}
          defaultPageSize={20}
        />
      </Fragment>
    );
  }
}


export default SourceProductDetail;
