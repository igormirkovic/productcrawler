import React, {Component, Fragment} from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import TreeMenu from 'react-simple-tree-menu';
import ReactTable from 'react-table';
import {Button, Modal} from 'react-bootstrap';
import ProductDetail from './ProductDetail';
import SourceTabs from './SourceTabs';
import matchSorter from 'match-sorter'
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";


class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            categories: [],
            edit: false,
            currentProducts: [],
            nodes: [],
            showModal: false,
            currentProduct: null,
            currentCategory: null,
            showModalAdd: false,
            addingSource: null,
            update: null,
            selectedProduct: null,
            deleteModal: false,
            removeProduct: null,
            sourceProducts: [],
            expanded: {},
            loaded: false,
            filtered: [],
            filterAll: ''

        };
    }

    componentDidMount() {
        fetch('/api/categories/')
            .then(res => res.json())
            .then(res => this.setState({categories: res}))
            .then(() => this.makeTree())
            .then(() => this.setState({expanded: null}));
    }

    makeTree = () => {
        const allNodes = {};
        const nodes = [];

        this.state.categories.forEach((cat, index) => {
            const curNode = {
                index: cat.id,
                key: cat.id,
                label: cat.name,
                original: cat,
                nodes: [],
                height: 0
            };

            if (cat.parent_id == null) {
                nodes.push(curNode);
                allNodes[cat.id] = curNode;
            } else {
                const parent = allNodes[cat.parent_id];
                if (parent) {
                    parent.nodes.push(curNode);
                    allNodes[cat.id] = curNode;
                }
            }
        });

        nodes.unshift({
            index: -1,
            key: -1,
            label: 'Favorites',
            nodes: []
        })

        this.setState({nodes});
    };

    filterAll = (e) => {
        const {value} = e.target;
        const filterAll = value;
        const filtered = [{id: 'all', value: filterAll}];
        this.setState({filterAll, filtered});
    }


    getProducts = (e) => {
        console.log('im here')
        if (e == -1) {
            window.globalHelper.fetchData(`/api/favoritesProducts`, (res) => {
                this.setState({
                    products: res,
                    edit: true
                });
            });
        } else {
            window.globalHelper.fetchData(`/api/productsbycategoryid/${e}`, (res) => {
                this.setState({
                    products: res,
                    edit: true
                });
            });
        }
    };

    onFilterChange = (filtered) => {
        if (filtered.length > 1 && this.state.filterAll.length) {
            // NOTE: this removes any FILTER ALL filter
            const filterAll = '';
            this.setState({filtered: filtered.filter(item => item.id != 'all'), filterAll});
        } else this.setState({filtered});
    }

    fetchProductDetail = (row) => {
        window.globalHelper.fetchData(`/api/products/${row.original.id}`, (res) => {
            this.setState({
                addOrView: 0,
                currentProduct: res[0],
                showModal: true,
                loaded: true
            });
        });
    };

    renderPriceCell = (cellInfo, sourceid) => {
        if (cellInfo && cellInfo.value) {
            return (`${cellInfo.value},00 ден`);
        }
        return <Button onClick={() => this.setState({
            showModalAdd: true,
            addingSource: sourceid,
            currentProduct: cellInfo.original
        })} size="sm" className="add-button">Add</Button>;
    };


    openRemoveModal = (row) => {
        this.setState({deleteModal: true, removeProduct: row.original.id});
    };

    removeProduct = () => {
        window.globalHelper.postData(`/api/sourceproducts/${this.state.removeProduct}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }, () => {
            this.getProducts(this.state.currentCategory);
            this.setState({
                deleteModal: false,
                removeProduct: null
            });
            this.forceUpdate();
        });
    };


    addProducts = () => {
        const data = {
            insertID: this.state.currentProduct,
            updateID: this.state.selectedProduct
        };
        window.globalHelper.postData('/api/sourceproducts', {
            method: 'PUT',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }, () => {
            this.getProducts(this.state.currentCategory);
            this.setState({
                showModalAdd: false,
                currentProduct: null,
                selectedProduct: null
            });
            this.forceUpdate();
        });
    };


    categoryClick = (e) => {
        this.state.currentCategory = e.index;
        this.getProducts(this.state.currentCategory);
    };

    handleModal = () => {
        this.setState({showModalAdd: false});
    };

    handleCheck = (row) => {
        let id = row.original.id;
        if (row.original.favorites == 0 || row.original.favorites == null) {
            window.globalHelper.postData('/api/favoritesProducts/', {
                method: 'POST',
                body: JSON.stringify({id}),
                headers: {
                    'Content-Type': 'application/json'
                }
            }, () => this.getProducts(this.state.currentCategory))
        } else {
            window.globalHelper.postData('/api/favoritesProductsOf/', {
                method: 'POST',
                body: JSON.stringify({id}),
                headers: {
                    'Content-Type': 'application/json'
                }
            }, () => this.getProducts(this.state.currentCategory))
        }


    }


    render() {

        let productdetail = null;
        let addsourceproduct = null;
        let removeproduct = null;
        if (this.state.currentProduct != null && this.state.loaded == true) {
            productdetail = (
                <ProductDetail
                    currentCategory={this.state.currentCategory}
                    onFetchProducts={this.getProducts}
                    key={`Small product detail ${this.state.currentProduct.id}`}
                    productDetail={this.state.currentProduct}
                />
            );
        }
        if (this.state.currentProduct != null) {
            addsourceproduct = [<SourceTabs
                onFetchProducts={this.getProducts}
                currentCategory={this.state.currentCategory}
                onHandleModal={this.handleModal}
                productDetail={this.state.currentProduct}
                addingMode
                source_id={this.state.addingSource}
                key={`Add source product${this.state.currentProduct.id}`}
            />, <Button onClick={() => this.addProducts()}
                        className={this.state.selectedProduct ? 'add-product-button' : 'hide'}>Add</Button>];
        }
        if (this.state.deleteModal) {
            removeproduct = (
                <Modal
                    size="sm"
                    show={this.state.deleteModal}
                    onHide={() => this.setState({deleteModal: false, removeProduct: null})}
                    aria-labelledby="example-modal-sizes-title-lg"
                    key="ModalProductrRemove"
                    className="remove-modal"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Are you sure you want to delete this product ?
                        <Button onClick={() => this.removeProduct()} className="view-button">Yes</Button>
                        <Button onClick={() => this.setState({deleteModal: false, removeProduct: null})}
                                className="remove-button">No</Button>
                    </Modal.Body>
                </Modal>
            );
        }

        return (
            <Fragment>
                {removeproduct}
                <Modal
                    size="xl"
                    show={this.state.showModal}
                    onHide={() => this.setState({showModal: false})}
                    aria-labelledby="example-modal-sizes-title-lg"
                    key="ModalProductView"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            {this.state.currentProduct != null ? this.state.currentProduct.name : ''}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className={'modal-body-config'}>
                        {productdetail}
                    </Modal.Body>
                </Modal>
                <Modal
                    size="lg"
                    show={this.state.showModalAdd}
                    onHide={() => this.setState({showModalAdd: false})}
                    aria-labelledby="example-modal-sizes-title-lg"
                    key="ModalProductAdd"
                >
                    <Modal.Header closeButton>
                        <Modal.Title id="example-modal-sizes-title-lg">
                            Add
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body className={'modal-config'}>
                        {addsourceproduct}
                    </Modal.Body>
                </Modal>
                <div className="container-table">
                    <div className="search-icon"/>
                    <TreeMenu
                        data={this.state.nodes}
                        onClickItem={(e) => {
                            this.categoryClick(e);
                        }}
                    />
                </div>
                <div className={this.state.edit ? 'showMe' : 'hide'}>
                    <div className='table-content'>
                        <div className='filter-found'>
                            <div className='search-icon'/>
                            <input placeholder='Type and search' value={this.state.filterAll}
                                   onChange={this.filterAll}/>
                        </div>
                        <ReactTable
                            resizable={false}
                            SubComponent={row => (
                                <Fragment>
                                    <div className="rt-tr-group" role="rowgroup">
                                        <div className="rt-tr" role="row">
                                            <div className="number">
                                                Tinex
                                            </div>
                                            <div className="rt-td first-col" role="gridcell"
                                                 style={{width: 300, maxWidth: 300}}>
                                                {row.original.sp1name ? row.original.sp1name : '/'}
                                            </div>
                                            <div className="rt-td" role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                {row.original.sp1price ? `${row.original.sp1price},00 ден` : '/'}
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td my-options" role="gridcell">
                                                <Button
                                                    size="sm"
                                                    onClick={() => {
                                                        this.fetchProductDetail(row);
                                                    }}
                                                    className="view-button"
                                                >
                                                    View
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="rt-tr-group" role="rowgroup">
                                        <div className="rt-tr" role="row">
                                            <div className="number">
                                                Paket
                                            </div>
                                            <div className="rt-td first-col" role="gridcell"
                                                 style={{width: 300, maxWidth: 300}}>
                                                {row.original.sp2name ? row.original.sp2name : '/'}
                                            </div>
                                            <div className="rt-td" role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                {row.original.sp2price ? `${row.original.sp2price},00 ден` : '/'}
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td my-options" role="gridcell">
                                                <Button
                                                    size="sm"
                                                    onClick={() => {
                                                        this.fetchProductDetail(row);
                                                    }}
                                                    className="view-button"
                                                >
                                                    View
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="rt-tr-group" role="rowgroup">
                                        <div className="rt-tr" role="row">
                                            <div className="number">
                                                Reptil
                                            </div>
                                            <div className="rt-td first-col" role="gridcell"
                                                 style={{width: 300, maxWidth: 300, flexGrow: 0}}>
                                                {row.original.sp3name ? row.original.sp3name : '/'}
                                            </div>
                                            <div className="rt-td" role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                /
                                            </div>
                                            <div className="rt-td " role="gridcell" style={{width: 100, maxWidth: 100}}>
                                                {row.original.sp3price ? `${row.original.sp3price},00 ден` : '/'}
                                            </div>
                                            <div className="rt-td my-options" role="gridcell">
                                                <Button
                                                    size="sm"
                                                    onClick={() => {
                                                        this.fetchProductDetail(row);
                                                    }}
                                                    className="view-button"
                                                >
                                                    View
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                </Fragment>
                            )}
                            data={this.state.products}
                            ref={(r) => {
                                this.selectTable = r;
                            }}
                            filtered={this.state.filtered}
                            defaultFilterMethod={(filter, row) =>
                                String(row[filter.id].name) === filter.value}
                            onFilteredChange={(e) => this.onFilterChange(e)}
                            columns={[
                                {
                                    Header: 'Name',
                                    accessor: d => d.name,
                                    columns: [{

                                        Header: '',
                                        width: 300,
                                        accessor: 'name',
                                        filterMethod: (filter, row) => {
                                            return row[filter.id].name.includes(filter.value);
                                        }
                                    }]
                                },
                                {
                                    Header: 'Price',
                                    columns: [{
                                        Header: 'Tinex',
                                        width: 100,
                                        accessor: 'sp1price',
                                        Cell: row => this.renderPriceCell(row, 1)
                                    }, {
                                        Header: 'Paket',
                                        accessor: 'sp2price',
                                        width: 100,
                                        Cell: row => this.renderPriceCell(row, 2)
                                    }, {
                                        Header: 'Reptil',
                                        accessor: 'sp3price',
                                        width: 100,
                                        Cell: row => this.renderPriceCell(row, 3)
                                    }],
                                    accessor: 'price'
                                },
                                {
                                    Header: 'Options',
                                    className: 'options-products',
                                    Cell: row => (
                                        <div>
                                            <Button
                                                size="sm"
                                                onClick={() => {
                                                    this.fetchProductDetail(row);
                                                }}
                                                className="view-button"
                                            >
                                                View
                                            </Button>
                                            <Button onClick={() => this.openRemoveModal(row)} size="sm"
                                                    className="remove-button">Remove</Button>
                                        </div>
                                    )
                                },
                                {
                                    Header: 'Favorites',
                                    width: 150,
                                    className: 'favorites-column',
                                    Cell: row =>
                                        <FormControlLabel className='favorites_control'
                                            control={<Checkbox checked={row.original.favorites == 1}
                                                               onChange={() => this.handleCheck(row)}
                                                               icon={<i className="material-icons">star_border</i>}
                                                               checkedIcon={<i
                                                                   className="material-icons">
                                                                   star
                                                               </i>} value="checkedH"/>}
                                        />
                                },
                                {
                                    Header: "All",
                                    id: 'all',
                                    show: false,
                                    width: 0,
                                    resizable: false,
                                    sortable: false,
                                    Filter: () => {
                                    },
                                    getProps: () => {
                                        return {
                                            // style: { padding: "0px"}
                                        }
                                    },
                                    filterMethod: (filter, rows) => {
                                        // using match-sorter
                                        // it will take the content entered into the "filter"
                                        // and search for it in EITHER the firstName or lastName
                                        const result = matchSorter(rows, filter.value, {
                                            keys: [
                                                "name",
                                            ], threshold: matchSorter.rankings.CONTAINS
                                        });
                                        return result;
                                    },
                                    filterAll: true,
                                },
                            ]}
                            defaultPageSize={15}
                        />
                    </div>
                </div>
            </Fragment>
        );
    }
}


export default Products;
