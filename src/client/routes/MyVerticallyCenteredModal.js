import { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import Form from 'react-bootstrap/es/Form';
import React from 'react';

class MyVerticallyCenteredModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      onSave: this.props.onSave
    };
  }

  saveName = () => {
    this.state.onSave(this.state.name);
  };


  render() {

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Add category
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={'modal-body-config'}>
          <Form.Group controlId="exampleForm.ControlInput1">
            <Form.Control defaultValue={this.props.name}
                          onChange={(e) => this.setState({ name: e.target.value })}
                          placeholder="Name"/>
          </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={this.saveName}>Save</Button>
          <Button variant="danger" onClick={this.props.onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default MyVerticallyCenteredModal;
