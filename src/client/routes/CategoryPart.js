import React, { Component, Fragment } from 'react';
import ReactTable from 'react-table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import TreeMenu from 'react-simple-tree-menu';
import {Dropdown, ButtonGroup} from 'react-bootstrap';



class CategoryPart extends Component {
  constructor(props){
    super(props);

    this.state = {
      products : [],
      modalShow: false,
      categories: [],
      currentCategory: null,
      clickedRow: null,
      onSelectProducts: this.props.onSelectProducts,
      source_id : null
    }
  }

  hideModal = () =>{
    this.setState({modalShow: false})
  };


  makeTree = () => {
    const allNodes = {};
    const nodes = [];

    this.state.categories.forEach((cat, index) => {
      const curNode = {
        index: cat.id,
        key: cat.id,
        label: cat.name,
        original: cat,
        nodes: []
      };

      if (cat.parent_id == null) {
        nodes.push(curNode);
        allNodes[cat.id] = curNode;
      }
      else {
        const parent = allNodes[cat.parent_id];
        if (parent) {
          parent.nodes.push(curNode);
          allNodes[cat.id] = curNode;
        }
      }
    });
    this.setState({nodes});
  };



  fetchProducts = () => {
    if (this.props.sourceid || this.state.source_id) {
      fetch('/api/getProductsByCategoryID/' + this.state.currentCategory.index)
        .then(res => res.json())
        .then(res => this.setState({ products: res }))
        .then(() => this.setState({ modalShow: false }))
    } else {
      fetch('/api/productsbycategoryid/' + this.state.currentCategory.index)
        .then(res => res.json())
        .then(res => this.setState({ products: res }))
        .then(() => this.setState({ modalShow: false }))
    }

  };

  fetchOurCategories = () =>{
    fetch('/api/categories/')
      .then(res => res.json())
      .then(res => this.setState({categories: res}))
      .then(() => this.makeTree())
      .then(() => this.setState({modalShow: true}))
  };

  fetchCategoriesOneProduct = () =>{
    fetch('/api/getCategoriesBySourceID/' + this.state.source_id)
      .then(res => res.json())
      .then(res => this.setState({categories: res}))
      .then(() => this.makeTree())
      .then(() => this.setState({modalShow: true}))
  };


  fetchCategories = () =>{
    fetch('/api/getCategoriesBySourceID/' + this.props.sourceid)
      .then(res => res.json())
      .then(res => this.setState({categories: res}))
      .then(() => this.makeTree())
      .then(() => this.setState({modalShow: true}))
  };


  onFetchCategories = () =>{
    if(this.props.OneProductOur){
      this.fetchOurCategories();
    }else{
      if(this.props.sourceid){
        this.fetchCategories()
      }else{
        this.fetchCategoriesOneProduct();
      }
    }
  };

  render() {
    console.log(this.props.source_id);
    return (
      <div className={this.props.styleOneProduct ? "category-part-product" : 'category-part'}>
        <div className='header-category-part'>
          <div className={this.props.dropDownShow ? "dropdown" : "hide"}>
            <Dropdown as={ButtonGroup}>
              <Button variant="success" onClick={() => this.fetchCategoriesOneProduct()}>{this.state.currentCategory ? this.state.currentCategory.label : 'Select category'}</Button>
              <Dropdown.Toggle split variant="success" id="dropdown-split-basic" />
              <Dropdown.Menu>
                <Dropdown.Item onClick={() => this.setState({source_id: 1})}>Tinex</Dropdown.Item>
                <Dropdown.Item onClick={() => this.setState({source_id: 2})}>Paket</Dropdown.Item>
                <Dropdown.Item onClick={() => this.setState({source_id: 3})}>Reptil</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
          <div className={this.props.dropDownShow ? "hide" : "category-name"} onClick={this.onFetchCategories}>{this.state.currentCategory ? this.state.currentCategory.label : 'Select category'}</div>
          <Modal
            size="lg"
            show={this.state.modalShow}
            onHide={this.hideModal}
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <Modal.Header closeButton>
              <Modal.Title id="example-modal-sizes-title-lg">
                Categories from {this.props.title}
              </Modal.Title>
              <Button className="select-category" onClick={this.fetchProducts} variant="success">Select</Button>
            </Modal.Header>
            <Modal.Body className='modal-body-config'>
              <TreeMenu data={this.state.nodes} onClickItem={(currentCategory) => this.setState({currentCategory})} />
            </Modal.Body >
          </Modal>
        </div>
        <ReactTable
          getTdProps={(state, rowInfo, column, instance) => {
            return {
              onClick: (e, handleOriginal) => {
                this.setState({clickedRow: rowInfo.original}, () => console.log(this.state.clickedRow));
                this.props.onSelectProducts(rowInfo.original);
                if (handleOriginal) {
                  handleOriginal()
                }
              }
            }
          }}
          getTrGroupProps={(state, rowInfo, column) => {
            if (rowInfo && this.state.clickedRow === rowInfo.original) {
              return {
                className: 'rt-tr-edit rt-tr-new'
              };
            }
            return {};
          }}

          filterable
          className='allsources'
          data={this.state.products}
          columns={[
            {
              Header: 'Name',
              accessor: 'name',
            }
          ]}
          defaultPageSize={100}
        />

      </div>

    );
  }
}


export default CategoryPart;
