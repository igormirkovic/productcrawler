import React, { Component, Fragment } from 'react';
import '../style/products.scss';
import 'react-table/react-table.css';
import {Button, Tabs, Tab, Image} from 'react-bootstrap';
import SmallSourceProductDetail from './SmallSourceProductDetail';
import Modal from 'react-bootstrap/es/Modal';
import TreeMenu from 'react-simple-tree-menu';
import SourceTabs from './SourceTabs';
import ReactSearchBox from 'react-search-box'
import { Chart } from 'react-charts';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";





class ProductDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      NameInput: '',
      ProductIDInput: '',
      PackageInput: '',
      WeightInput: '',
      UnitOfMeasureInput: null,
      ProducerInput: '',
      ProductTypeInput: '',
      DescriptionInput: '',
      sourceProducts: null,
      modalShow: false,
      categories: [],
      showModal: false,
      currentProduct: null,
      source_id: null,
      nodes: [],
      clickedRow: null,
      products: [],
      key: 'tinex',
      selectedProduct: null,
      logs: [],
      hardMaxChart: 0,
      productDetail: null,
        Favorites: 0,
        Picture: null
    }

  }
  handleDelete = (item) => {
    var array = [...this.state.sourceProducts];
    var index = array.indexOf(item);
    if (index !== -1) {
      array.splice(index, 1);
      this.setState({ sourceProducts: array });
    }
  };

  componentDidMount() {
    this.fetchProductsSmall();

  }

  getProductDetail = (param)=>{
    window.globalHelper.fetchData('/api/getproductdetail/' + this.props.productDetail.id,(res) =>{
        console.log(res);
         this.setState(
             {
                 productDetail: res,
                 NameInput: res[0]['name'],
                 ProductIDInput: res[0]['product_id'],
                 PackageInput: res[0]['package'],
                 WeightInput: res[0]['weight'],
                 UnitOfMeasureInput: res[0]['unit_of_measure'],
                 ProducerInput: res[0]['producer'],
                 ProductTypeInput: res[0]['product_type'],
                 DescriptionInput: res[0]['description'],
                 Favorites: res[0]['favorites']
             },() => {
                 this.fetchPictureProductDetail()
                 if(param !== false){
                     this.getProductLogs()
                 }
             })
    })
  }


  fetchPictureProductDetail = () =>{
      console.log(' im hereeeeeeeeeeeeeeeeeeeee')
    window.globalHelper.fetchData('/api/getPictureProductDetail/' +this.props.productDetail.id, (res) =>{
        console.log(res['image_url'])
        this.setState({Picture: res[0]['image_url']})
    })
  }


  getProductLogs = () =>{
    let { hardMaxChart } = this.state;
    this.state.sourceProducts.forEach((product, index) => {
      hardMaxChart += product.price;
      fetch('/api/sourceproductslogs/' + product.id)
        .then(res => res.json())
        .then((res) => {
          res.forEach((element) => {
            let a = element.crawl_date.split("T");
            let date = a[0];
            let dateArr = date.split("-");
            let year = dateArr[0];
            let mounth = dateArr[1];
            let day = dateArr[2];
            let time = a[1];
            let timeArr = time.split(":");
            let hour = timeArr[0];
            let minute = timeArr[1];
            let seconds = timeArr[2].slice(0, 2);
            element.crawl_date = day + '/' + mounth + '/' + year + " " + hour + ':' + minute + ':' + seconds;
            this.setState(prevState => ({
              logs: [...prevState.logs, element]
            , hardMaxChart}))
          })
          })
    })
  };

  fetchProductsSmall = () => {
    window.globalHelper.fetchData('/api/sourceproductsProductID/' + this.props.productDetail.id, (res) =>{
      this.setState({ sourceProducts: res });
        this.getProductDetail();

    });
  };

  addProduct = () => {
    let data = {
      insertID: this.props.productDetail.id,
      updateID: this.state.selectedProduct
    };
    window.globalHelper.postData('/api/sourceproducts', {
      method: 'PUT',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    }, () =>{
      this.fetchProductsSmall();
      this.setState({modalShow: false})
    })
  };

  handleModal = () =>{
    this.setState({modalShow: false});
  };

  handleSourceProducts = (sourceProducts) =>{
    this.setState({sourceProducts})
  };

  makeData = () =>{
    const example = [
      [[1, 10], [2, 10], [3, 10]],
      [[1, 10], [2, 10], [3, 10]],
      [[1, 10], [2, 10], [3, 10]]
    ];
    let data = [];

    let finaldata = [];
    this.state.sourceProducts.forEach((product,index) =>
    {
      let obj = {};
      data = []
      switch (product.source_id) {
        case 1:
          obj['label'] = 'Tinex';
          break;
        case 2:
          obj['label'] = 'Paket';
          break;
        case 3:
          obj['label'] = 'Reptil'
          break;
      }
      this.state.logs.forEach((log,index) =>{
        if(product.id === log.sourceproductid) {
          let dataLog = {};
          let days = log.crawl_date.split(' ')[0].split('/')[0];
          let months = log.crawl_date.split(' ')[0].split('/')[1];
          let year = log.crawl_date.split(' ')[0].split('/')[2];
          let dateString = year + '-' + months + '-' + days;
          let date = new Date(dateString);
          dataLog['x'] = (new Date(parseInt(year), parseInt(months - 1), parseInt(days)));
          dataLog['y'] = (log.price);
          data.push(dataLog);
        }
      });
          obj['data'] = data
          finaldata.push(obj);

    });

    return finaldata;
  };


    handleCheck = () => {
          let id = this.props.productDetail.id;
          if (this.state.Favorites === 0 || this.state.Favorites == null) {
              window.globalHelper.postData('/api/favoritesProducts/', {
                  method: 'POST',
                  body: JSON.stringify({id}),
                  headers: {
                      'Content-Type': 'application/json'
                  }
              }, () => this.getProductDetail(false))
          } else {
              window.globalHelper.postData('/api/favoritesProductsOf/', {
                  method: 'POST',
                  body: JSON.stringify({id}),
                  headers: {
                      'Content-Type': 'application/json'
                  }
              }, () => this.getProductDetail(false))
          }
    }

  addProductDetails = () =>{
    let data = {
      id: this.props.productDetail.id,
      name: this.state.NameInput,
      product_id: this.state.ProductIDInput,
      package: this.state.PackageInput,
      weight: this.state.WeightInput,
      unit_of_measure: this.state.UnitOfMeasureInput,
      producer: this.state.ProducerInput,
      product_type: this.state.ProductTypeInput,
      description: this.state.DescriptionInput,
    }

    window.globalHelper.postData('/api/productdetails',{
      method: 'PUT',
      body: JSON.stringify({data}),
      headers: {
        'Content-Type': 'application/json'
      }
    }
        ,() =>{

    })
  }

  render() {
    let smallsourceproducts = [];
    if (this.state.sourceProducts != null) {
      for (let i = 0; i < this.state.sourceProducts.length; i++) {
        smallsourceproducts.push(<SmallSourceProductDetail
          key={'Small product detail' + this.state.sourceProducts[i].id}
          productDetail={this.state.sourceProducts[i]}
          onRemove={(item) => this.handleDelete(item)}/>)
      }
    }


    let favorites = null;
    if(this.state.productDetail !== null){
      favorites = <FormControlLabel className='favorites_control'
                                    control={<Checkbox checked={this.state.Favorites === 1}
                                                       onChange={() => this.handleCheck()}
                                                       icon={<i className="material-icons">star_border</i>}
                                                       checkedIcon={<i
                                                           className="material-icons">
                                                           star
                                                       </i>} value="checkedH"/>}
                                    label="Favorite"
      />
    }




    let chart = null;
    if(this.state.sourceProducts && this.state.logs){
      chart = <div className='chart-div-products'>
        <Chart
          series={3}
          data={this.makeData()}
          axes={[
            { primary: true, type: "time", position: "bottom" },
            { type: "linear", position: "left", hardMax: this.state.hardMaxChart, hardMin: 0 }
          ]}
          tooltip={true}
        />
      </div>
    }


    return (
      <Fragment>
        <Modal
          size="xl"
          show={this.state.modalShow}
          onHide={
            () => {
              this.props.onFetchProducts(this.props.currentCategory);
              this.setState({ modalShow: false })
            }
          }
          aria-labelledby="example-modal-sizes-title-xl"
        >
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-xl">
              Add product
            </Modal.Title>
          </Modal.Header>
          <Modal.Body className={'modal-body-config'}>
            <Button onClick={this.addProduct} className={this.state.selectedProduct ? "add-product-button" : "hide"}>Add</Button>
            <Tabs
              id="controlled-tab-example"
              activeKey={this.state.key}
              onSelect={key => this.setState({ key, selectedProduct: null })}
            >
              <Tab eventKey='tinex' title="Tinex">
                <SourceTabs currentCategory={this.props.currentCategory}
                            onFetchProducts={this.props.onFetchProducts}
                            handleModal={this.handleModal}
                            handleSourceProducts={this.handleSourceProducts}
                            productDetail={this.props.productDetail}
                            source_id={1}/>
              </Tab>
              <Tab eventKey='paket' title="Paket">
                <SourceTabs
                    currentCategory={this.props.currentCategory}
                    onFetchProducts={this.props.onFetchProducts}
                    handleModal={this.handleModal}
                    handleSourceProducts={this.handleSourceProducts}
                    productDetail={this.props.productDetail}
                    source_id={2}/>
              </Tab>
              <Tab eventKey='reptil' title="Reptil">
                <SourceTabs
                    currentCategory={this.props.currentCategory}
                    onFetchProducts={this.props.onFetchProducts}
                    handleModal={this.handleModal}
                    handleSourceProducts={this.handleSourceProducts}
                    productDetail={this.props.productDetail}
                    source_id={3}/>
              </Tab>
            </Tabs>
          </Modal.Body>
        </Modal>
        <div className="product-detail-title">
          {this.props.productDetail.name}
        </div>
          {favorites}
        <Button className="add-button-detail" onClick={() => this.setState({ modalShow: true })}>Add
          source product</Button>
          <div className='block'>
              <div className='block-image-form'>
              {chart}
              <Image className='image-url product-detail-pic' src={this.state.Picture} rounded/>
              </div>
              <div className='product-form'>
          <TextField
              value={this.state.ProductIDInput}
              type='number'
              onChange={(e) => this.setState({ProductIDInput: e.target.value})}
            id="standard-dense"
            className='product-id-input'
            label="Product ID"
            margin="dense"
        />
          <TextField
              value={this.state.NameInput}
              onChange={(e) => this.setState({NameInput: e.target.value})}
              id="standard-dense"
              className='name-input'
              label="Name"
              margin="dense"
          />
            <FormControl className='package-input'>
                <InputLabel htmlFor="age-helper">{this.state.PackageInput !== '' ? '' : 'Package'}</InputLabel>
                <Select
                    value={this.state.PackageInput }
                    onChange={(e) => this.setState({PackageInput : e.target.value})}
                    input={<Input name="age" id="age-helper" />}
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    <MenuItem value={'Kutija'}>Kutija</MenuItem>
                    <MenuItem value={'Paket'}>Paket</MenuItem>
                    <MenuItem value={'Shishe'}>Shishe</MenuItem>
                    <MenuItem value={'Tetrapak'}>Tetrapak</MenuItem>
                    <MenuItem value={'Limenka'}>Limenka</MenuItem>
                    <MenuItem value={'Kjesa'}>Kjesa</MenuItem>
                    <MenuItem value={'Drugo'}>Drugo</MenuItem>

                </Select>
                <FormHelperText>Package</FormHelperText>
            </FormControl>
          <TextField
              value={this.state.WeightInput}
              type='number'
              onChange={(e) => this.setState({WeightInput: e.target.value})}
              id="standard-dense"
              className='weight-input'
              label="Weight"
              margin="dense"
          />
          <FormControl className='unit-input'>
            <InputLabel htmlFor="age-helper">{this.state.UnitOfMeasureInput !== '' ? '' : 'Unit of measure'}</InputLabel>
            <Select
                value={this.state.UnitOfMeasureInput}
                onChange={(e) => this.setState({UnitOfMeasureInput: e.target.value})}
                input={<Input name="age" id="age-helper" />}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              <MenuItem value={'kg'}>kg</MenuItem>
                <MenuItem value={'g'}>g</MenuItem>
              <MenuItem value={'l'}>l</MenuItem>
                <MenuItem value={'ml'}>ml</MenuItem>
              <MenuItem value={'parce'}>parce</MenuItem>
            </Select>
            <FormHelperText>Unit</FormHelperText>
          </FormControl>
          <TextField
              value={this.state.ProducerInput}
              id="standard-dense"
              onChange={(e) => this.setState({ProducerInput: e.target.value})}
              className='producer-input'
              label="Producer"
              margin="dense"
          />
            <FormControl className='unit-input'>
                <InputLabel htmlFor="age-helper">{this.state.ProductTypeInput !== '' ? '' : 'Unit of measure'}</InputLabel>
                <Select
                    value={this.state.ProductTypeInput}
                    onChange={(e) => this.setState({ProductTypeInput: e.target.value})}
                    input={<Input name="age" id="age-helper" />}
                >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>`````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````````
                    <MenuItem value={'Higiena'}>Higiena</MenuItem>
                    <MenuItem value={'Sokovi i voda'}>Sokovi i voda</MenuItem>
                    <MenuItem value={'Kafe i caj'}>Kafe i caj</MenuItem>
                    <MenuItem value={'Hemija'}>Hemija</MenuItem>
                    <MenuItem value={'Konditorija'}>Konditorija</MenuItem>
                    <MenuItem value={'Drugo'}>Drugo</MenuItem>
                </Select>
                <FormHelperText>Product type</FormHelperText>
            </FormControl>
          <TextField
              id="standard-multiline-static"
              value={this.state.DescriptionInput}
              onChange={(e) => this.setState({DescriptionInput: e.target.value})}
              className='description-input'
              label="Description"
              multiline
              rows="4"
              defaultValue="Default Value"
              margin="normal"
          />
          <Button onClick={() => this.addProductDetails()} className='save-button'>Save</Button>

        </div>
          </div>
        <div className="sourcecategories-block">
          {smallsourceproducts}
        </div>
      </Fragment>
    );
  }
}



export default ProductDetail;
