import React, { Component } from 'react';
import './style/app.css';
import { HashRouter, Switch, Route, } from 'react-router-dom';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import Reptil from './routes/Reptil';
import Tinex from './routes/Tinex';
import Paket from './routes/Paket';
import Categories from './routes/Categories';
import AllSources from './routes/AllSources'
import Products from './routes/Products';
import OneProduct from './routes/OneProduct';
import Crawl from './routes/CrawlerComponent/Crawl';
import Helper from './helper';
import CrawlerFinal from './routes/CrawlerComponent/CrawlerFinal';
// const TinexCrawler = require('./../server/crawler/Tinex/tinex');
// const PaketCrawler = require('./../server/crawler/Paket/paketi');
// const ReptilCrawler = require('./../server/crawler/Reptil/reptil');


window.globalHelper = Helper;

export default class App extends Component {
  constructor(){
    super();

    this.state = {
      spinner: false
    };

    window.globalHelper.spinnerStart = this.runSpinner;
    window.globalHelper.spinnerEnd = this.stopSpinner;
  }

  runSpinner = () => {
    this.setState({spinner: true})
    console.log('start');
  };

  stopSpinner = () => {
    this.setState({spinner: false})
    console.log('stop');
  };

  render() {
    return (
      <HashRouter >
        <div>
          <Navbar>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#products">Products and categories</Nav.Link>
              <NavDropdown title="Sources" id="basic-nav-dropdown">
                <NavDropdown.Item href="#tinex">Tinex</NavDropdown.Item>
                <NavDropdown.Item href="#paket">Paket</NavDropdown.Item>
                <NavDropdown.Item href="#reptil">Reptil</NavDropdown.Item>
              </NavDropdown>
              <Nav.Link href="#crawl">Crawl</Nav.Link>
            </Nav>
            </Navbar.Collapse>
          </Navbar>
          <Switch>
            <Route exact path="/" component={Products} />
            <Route exact path="/tinex" component={Tinex} />
            <Route exact path="/paket" component={Paket} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/reptil" component={Reptil} />
            <Route exact path="/categories" component={Categories} />
            <Route exact path="/allsources" component={AllSources} />
            <Route exact path="/oneproduct" component={OneProduct} />
            <Route exact path="/crawl" component={CrawlerFinal}/>
          </Switch>
        </div>
        <div hidden={!this.state.spinner} className='spinner_container'>
          <div className='spinner_background'></div>
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </div>
      </HashRouter>
    );
  }
}
